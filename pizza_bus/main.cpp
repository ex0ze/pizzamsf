/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>

#include "../thirdparty/cppzmq/zmq.hpp"
#include "../thirdparty/cxxopts/include/cxxopts.hpp"

#include "from_json.hpp"

int main(int argc, char * argv[])
{
    std::string description = "Pizza Bus is service that implements inter-service message bus\n";
    cxxopts::Options options("Pizza Bus", std::move(description));

    options.add_options()
        ("from-json", "Json file path", cxxopts::value<std::string>())
        ("subs-addr", "Address for subscribers connection", cxxopts::value<std::string>())
        ("pubs-addr", "Address for publishers connection", cxxopts::value<std::string>())
        ("h,help", "Show this help");
    cxxopts::ParseResult result;
    try {
        result = options.parse(argc, argv);
    }
    catch (std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }

    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        return 0;
    }
    if (result.count("from-json")) {
        try {
            auto args = from_json(result["from-json"].as<std::string>());
            zmq::context_t context(1);
            zmq::socket_t pub_socket(context, ZMQ_XPUB);
            zmq::socket_t sub_socket(context, ZMQ_XSUB);
            if (args.encryption()) {
                pub_socket.set(zmq::sockopt::curve_server, true);
                pub_socket.set(zmq::sockopt::curve_secretkey, *args.bus_pub_private_key());
                sub_socket.set(zmq::sockopt::curve_server, true);
                sub_socket.set(zmq::sockopt::curve_secretkey, *args.bus_sub_private_key());
            }
            pub_socket.bind(args.publishers_addr());
            sub_socket.bind(args.subscribers_addr());
            std::cout << "Service bus started" << std::endl;
            std::cout << "Connect subscribers to [ " << args.subscribers_addr() << " ]" << std::endl;
            std::cout << "Connect publishers to [ " << args.publishers_addr() << " ]" << std::endl;
            zmq::proxy(pub_socket, sub_socket);
            return 0;
        }
        catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
            return 1;
        }
    }
    if (result.count("subs-addr") == 0) {
        std::cout << "Missing --subs-addr option" << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    if (result.count("pubs-addr") == 0) {
        std::cout << "Missing --pubs-addr option" << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    try {
        std::string subscribers_addr = result["subs-addr"].as<std::string>();
        std::string publishers_addr = result["pubs-addr"].as<std::string>();
        zmq::context_t context(1);
        zmq::socket_t pub_socket(context, ZMQ_XPUB);
        zmq::socket_t sub_socket(context, ZMQ_XSUB);
        pub_socket.bind(subscribers_addr);
        sub_socket.bind(publishers_addr);
        std::cout << "Service bus started" << std::endl;
        std::cout << "Connect subscribers to [ " << subscribers_addr << " ]" << std::endl;
        std::cout << "Connect publishers to [ " << publishers_addr << " ]" << std::endl; 
        zmq::proxy(pub_socket, sub_socket);
        return 0;
    }
    catch (const std::exception& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
    } 
    return 0;
}
