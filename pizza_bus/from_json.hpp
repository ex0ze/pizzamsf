/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <filesystem>
#include <fstream>
#include <string>

#include <pizza-config/parse.hpp>

static pizza::bus from_json(const std::string& path) {
    if (!std::filesystem::exists(path))
        throw std::runtime_error(path + " does not exist");
    std::ifstream in(path);
    in.seekg(0, std::ios::end);
    std::size_t size = in.tellg();
    in.seekg(0, std::ios::beg);
    std::string buf;
    buf.resize(size);
    in.read(buf.data(), buf.size());
    in.close();
    auto result = pizza::parse(buf);
    if (result->app() != pizza::basic_config::application::bus)
        throw std::runtime_error("Wrong type of application, must be 'broker'");
    pizza::bus ret = *dynamic_cast<pizza::bus*>(result.get());
    return ret;
}
