TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../build/
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
SOURCES += \
        handlers/CollectRegularStatisticsHandler.cpp \
        handlers/LoadHandler.cpp \
        handlers/ReloadHandler.cpp \
        handlers/RestartHandler.cpp \
        handlers/RunHandler.cpp \
        handlers/StopHandler.cpp \
        handlers/UnloadHandler.cpp \
        main.cpp

HEADERS += \
    RunnerInstance.h \
    handlers/CollectRegularStatisticsHandler.h \
    handlers/LoadHandler.h \
    handlers/ReloadHandler.h \
    handlers/RestartHandler.h \
    handlers/RunHandler.h \
    handlers/StopHandler.h \
    handlers/UnloadHandler.h

LIBS += -lzmq

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build/release/ -lpizza-runner
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build/debug/ -lpizza-runner
else:unix: LIBS += -L$$PWD/../build/ -lpizza-runner

INCLUDEPATH += $$PWD/../pizza-runner/include
DEPENDPATH += $$PWD/../pizza-runner/include
