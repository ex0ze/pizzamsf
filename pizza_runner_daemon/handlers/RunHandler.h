#ifndef RUNHANDLER_H
#define RUNHANDLER_H

#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processRunMessage(msg::IMessage::Ptr request);

}
#endif // RUNHANDLER_H
