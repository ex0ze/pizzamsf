#ifndef COLLECTREGULARSTATISTICSHANDLER_H
#define COLLECTREGULARSTATISTICSHANDLER_H

#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processCollectRegularStatisticsMessage(msg::IMessage::Ptr request);

}
#endif // COLLECTREGULARSTATISTICSHANDLER_H
