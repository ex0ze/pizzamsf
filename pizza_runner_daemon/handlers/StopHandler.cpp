#include "StopHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/StopMessage.h>
#include <pizza-runner/RunnerConfig.h>

namespace handlers
{

pizza::runner::message::IMessage::Ptr processStopMessage(pizza::runner::message::IMessage::Ptr request)
{
    auto message = dynamic_cast<msg::RequestStopMessage*>(request.get());
    const auto& confName = message->name();

    bool ok = RunnerInstance::get().stopConfig(confName);

    auto replyBase = msg::ReplyStopMessage::create();
    auto reply = dynamic_cast<msg::ReplyStopMessage*>(replyBase.get());

    if (ok)
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Success).setWhat("Config " + confName + " successfully stopped");
    }
    else
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Failed).setWhat("Error stopping config " + confName);
    }
    return replyBase;
}

}
