#ifndef RELOADHANDLER_H
#define RELOADHANDLER_H


#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processReloadMessage(msg::IMessage::Ptr request);

}


#endif // RELOADHANDLER_H
