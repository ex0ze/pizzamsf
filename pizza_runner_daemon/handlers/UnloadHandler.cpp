#include "UnloadHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/UnloadMessage.h>
#include <pizza-runner/RunnerConfig.h>
namespace handlers {

pizza::runner::message::IMessage::Ptr processUnloadMessage(pizza::runner::message::IMessage::Ptr request)
{
    auto message = dynamic_cast<msg::RequestUnloadMessage*>(request.get());
    const auto& confName = message->name();
    auto replyBase = msg::ReplyUnloadMessage::create();
    auto reply = dynamic_cast<msg::ReplyUnloadMessage*>(replyBase.get());
    bool ok = RunnerInstance::get().removeConfig(confName);
    if (ok)
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Success).setWhat("Config " + confName + " successfully removed");
        return replyBase;
    }
    else
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Failed).setWhat("Config " + confName + " not found");
        return replyBase;
    }
}

}
