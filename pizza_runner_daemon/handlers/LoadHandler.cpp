#include "LoadHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/LoadMessage.h>
#include <pizza-runner/RunnerConfig.h>

namespace handlers
{

pizza::runner::message::IMessage::Ptr processLoadMessage(pizza::runner::message::IMessage::Ptr request)
{
    auto message = dynamic_cast<msg::RequestLoadMessage*>(request.get());
    std::string_view path = message->path();
    auto replyBase = msg::ReplyLoadMessage::create();
    auto reply = dynamic_cast<msg::ReplyLoadMessage*>(replyBase.get());
    namespace pr = pizza::runner;
    auto maybeConfig = pr::config::Config::fromFile(path);
    if (maybeConfig.has_value())
    {
        std::string confName = maybeConfig.value().configName();
        RunnerInstance::get().addConfig(std::move(maybeConfig.value()));
        reply->setStatus(msg::ReplyLoadMessage::MessageStatus::Success).setWhat("Loaded config " + confName);
        return replyBase;
    }
    else
    {
        reply->setStatus(msg::ReplyLoadMessage::MessageStatus::Failed).setWhat("Error loading config");
        return replyBase;
    }
}

}
