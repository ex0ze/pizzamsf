#ifndef RESTARTHANDLER_H
#define RESTARTHANDLER_H


#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processRestartMessage(msg::IMessage::Ptr request);

}

#endif // RESTARTHANDLER_H
