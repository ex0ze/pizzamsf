#include "RunHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/RunMessage.h>
#include <pizza-runner/RunnerConfig.h>

namespace handlers {

msg::IMessage::Ptr processRunMessage(msg::IMessage::Ptr request)
{
    auto message = dynamic_cast<msg::RequestRunMessage*>(request.get());
    const auto& confName = message->name();

    bool ok = RunnerInstance::get().runConfig(confName);

    auto replyBase = msg::ReplyRunMessage::create();
    auto reply = dynamic_cast<msg::ReplyRunMessage*>(replyBase.get());

    if (ok)
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Success).setWhat("Config " + confName + " successfully launched");
    }
    else
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Failed).setWhat("Error launching config " + confName);
    }
    return replyBase;
}

}
