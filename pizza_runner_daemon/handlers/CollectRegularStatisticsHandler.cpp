#include "CollectRegularStatisticsHandler.h"

#include "../RunnerInstance.h"
#include <pizza-runner/message/RequestRegularStatisticsMessage.h>
#include <pizza-runner/message/ReplyRegularStatisticsMessage.h>
#include <pizza-runner/stats/RegularStatisticsCollector.h>
#include <pizza-runner/RunnerConfig.h>

namespace handlers
{

pizza::runner::message::IMessage::Ptr processCollectRegularStatisticsMessage(pizza::runner::message::IMessage::Ptr)
{
    namespace stats = pizza::runner::statistics;
    auto replyBase = msg::ReplyRegularStatisticsMessage::create();
    auto reply = dynamic_cast<msg::ReplyRegularStatisticsMessage*>(replyBase.get());
    auto collector = stats::RegularStatisticsCollector::create(RunnerInstance::get());
    reply->setCollector(std::move(collector)).setStatus(msg::IReplyMessage::MessageStatus::Success);
    return replyBase;
}

}
