#ifndef LOADHANDLER_H
#define LOADHANDLER_H

#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processLoadMessage(msg::IMessage::Ptr request);

}
#endif // LOADHANDLER_H
