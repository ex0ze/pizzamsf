#ifndef UNLOADHANDLER_H
#define UNLOADHANDLER_H


#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processUnloadMessage(msg::IMessage::Ptr request);

}

#endif // UNLOADHANDLER_H
