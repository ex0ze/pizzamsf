#ifndef STOPHANDLER_H
#define STOPHANDLER_H

#include <pizza-runner/message/IMessage.h>
namespace handlers {


namespace msg = pizza::runner::message;

msg::IMessage::Ptr processStopMessage(msg::IMessage::Ptr request);

}

#endif // STOPHANDLER_H
