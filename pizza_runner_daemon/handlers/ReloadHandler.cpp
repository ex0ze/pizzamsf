#include "ReloadHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/ReloadMessage.h>
#include <pizza-runner/RunnerConfig.h>
namespace handlers {

pizza::runner::message::IMessage::Ptr processReloadMessage(pizza::runner::message::IMessage::Ptr request)
{
    namespace cfg = pizza::runner::config;
    auto message = dynamic_cast<msg::RequestReloadMessage*>(request.get());
    const auto& confName = message->name();
    auto replyBase = msg::ReplyReloadMessage::create(); // IMessage
    auto reply = dynamic_cast<msg::ReplyReloadMessage*>(replyBase.get()); // IMessage -> IReplyMessage -> ReplyReloadMessage
    const auto confPtr = RunnerInstance::get().config(confName);
    std::string err;
    if (confPtr)
    {
        const auto path = confPtr->path();
        //confPtr become invalid here
        bool ok = RunnerInstance::get().removeConfig(confName);
        if (ok)
        {
            auto newConf = cfg::Config::fromFile(path);
            if (newConf.has_value())
                RunnerInstance::get().addConfig(std::move(newConf.value()));
            else
            {
                err = "failed to load config " + confName;
            }
        }
        else
        {
            err = "config " + confName + " not found";
        }
        if (ok)
        {
            reply->setStatus(msg::IReplyMessage::MessageStatus::Success).setWhat("Config " + confName + " successfully reloaded");
            return replyBase;
        }
    }
    //fallback
    reply->setStatus(msg::IReplyMessage::MessageStatus::Failed).setWhat(err);
    return replyBase;
}

}
