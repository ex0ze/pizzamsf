#include "RestartHandler.h"
#include "../RunnerInstance.h"
#include <pizza-runner/message/RestartMessage.h>
#include <pizza-runner/RunnerConfig.h>

namespace handlers {

msg::IMessage::Ptr processRestartMessage(msg::IMessage::Ptr request)
{
    auto message = dynamic_cast<msg::RequestRestartMessage*>(request.get());
    const auto& confName = message->name();

    bool ok = RunnerInstance::get().stopConfig(confName);
    if (ok) ok = RunnerInstance::get().runConfig(confName);

    auto replyBase = msg::ReplyRestartMessage::create();
    auto reply = dynamic_cast<msg::ReplyRestartMessage*>(replyBase.get());

    if (ok)
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Success).setWhat("Config " + confName + " successfully restarted");
    }
    else
    {
        reply->setStatus(msg::IReplyMessage::MessageStatus::Failed).setWhat("Error restarting config " + confName);
    }
    return replyBase;
}

}
