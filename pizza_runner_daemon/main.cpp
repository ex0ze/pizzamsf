#include <iostream>
#include <filesystem>
#include <unistd.h>

#include <pizza-runner/local_server/LocalServer.h>
#include <pizza-runner/helpers.h>
#include <pizza-runner/ServerMessageFormat.hpp>
#include <pizza-runner/message/IMessage.h>
#include <pizza-runner/MessageHandlersRunner.h>
#include "../thirdparty/cxxopts/include/cxxopts.hpp"

#include "RunnerInstance.h"
#include "handlers/LoadHandler.h"
#include "handlers/UnloadHandler.h"
#include "handlers/RunHandler.h"
#include "handlers/StopHandler.h"
#include "handlers/RestartHandler.h"
#include "handlers/ReloadHandler.h"
#include "handlers/CollectRegularStatisticsHandler.h"

namespace pr = pizza::runner;
namespace ph = pizza::helpers;

void loadDirectory(std::string_view dir)
{
    namespace fs = std::filesystem;
    if (!fs::exists(dir))
        return;

    for (auto& e : fs::directory_iterator(dir))
    {
        auto& path = e.path();
        auto ext = path.extension().string();
        std::transform(std::begin(ext), std::end(ext), std::begin(ext), [](auto ch) { return std::tolower(ch); });
        if (ext == ".json")
        {
            std::cout << "Reading config " << path << std::endl;
            auto maybeConfig = pr::config::Config::fromFile(path.c_str());
            if (maybeConfig.has_value())
            {
                RunnerInstance::get().addConfig(std::move(maybeConfig.value()));
                std::cout << "Success\n";
            }
            else
                std::cout << "Failed\n";
        }
    }
}

#include <zmq.hpp>

int main(int argc, char* argv[])
{
    std::string description = "Pizza Runner Daemon is service that can:\n"
                              "1) Read JSON configuration files from some directory on startup and run appropriate services\n"
                              "2) Run commands from pizza-runner-cli";
    cxxopts::Options options("Pizza Runner Daemon", std::move(description));
    options.add_options()
        ("terminal", "Pizza Runner Daemon will start in terminal")
            ("daemon", "Pizza Runner Daemon will start as daemon")
                ("config-dir", "Path in which Pizza Runner Daemon will search and run configs at startup", cxxopts::value<std::string>()->default_value(""))
                    ("h,help", "Show this help");
    cxxopts::ParseResult result;
    try
    {
        result = options.parse(argc, argv);
    }
    catch (std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    if (result.count("help") > 0)
    {
        std::cout << options.help() << std::endl;
        return 0;
    }
    if (result.count("terminal") > 0 && result.count("daemon") > 0)
    {
        std::cout << "Error, choose terminal or daemon param, not both" << std::endl;
        return 1;
    }
    if (result.count("daemon") > 0)
        daemon(1, 1); // no change cwd, no change fds
    else
        std::cout << "Launching Pizza Runner Daemon in terminal attached mode" << std::endl;
    const auto& dir = result["config-dir"].as<std::string>();
    if (!dir.empty())
        loadDirectory(dir);
    pr::srv::LocalServer server(pr::srv::format::kDefaultDaemonAddr);
    pr::srv::handlers::HandlersRunner hRunner;
    using pkt = pr::message::IMessage::MessagePacket;
    using act = pr::message::IMessage::MessageAction;
    hRunner.registerHandler(pkt::Request, act::Load, handlers::processLoadMessage);
    hRunner.registerHandler(pkt::Request, act::Unload, handlers::processUnloadMessage);
    hRunner.registerHandler(pkt::Request, act::Run, handlers::processRunMessage);
    hRunner.registerHandler(pkt::Request, act::Stop, handlers::processStopMessage);
    hRunner.registerHandler(pkt::Request, act::Reload, handlers::processReloadMessage);
    hRunner.registerHandler(pkt::Request, act::Restart, handlers::processRestartMessage);
    hRunner.registerHandler(pkt::Request, act::CollectRegularStatistics, handlers::processCollectRegularStatisticsMessage);
    while (true)
    {
        auto recvMessage = server.recv();
        auto sendMessage = hRunner.processMessage(recvMessage);
        server.reply(sendMessage);
    }
    return 0;
}
