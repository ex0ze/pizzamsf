#ifndef RUNNERINSTANCE_H
#define RUNNERINSTANCE_H

#include <pizza-runner/helpers.h>
#include <pizza-runner/Runner.h>

using RunnerInstance = pizza::helpers::SingleInstanceOf<pizza::runner::Runner>;

#endif // RUNNERINSTANCE_H
