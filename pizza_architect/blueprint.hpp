/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <rapidjson/document.h>

#include <pizza-config/basic_config.hpp>
#include "key.hpp"

namespace pizza
{

class blueprint {
public:
    using config_map = std::unordered_map<std::string, std::unique_ptr<basic_config>>; // config name - config

    void parse(std::string str);
    void save(const std::string& path);
    constexpr bool encryption() const noexcept { return encryption_; }

    constexpr const config_map& configs() const noexcept { return configs_; }

private:
    std::string buf_;
    config_map configs_;
    std::vector<std::string> json_stacktrace_;

    bool encryption_ = false;

    void get_broker_list(rapidjson::Document& document);
    void get_bus_list(rapidjson::Document& document);
    void get_services_list(rapidjson::Document& document);

    void normalize_addresses();

    void normalize_keys();
};

}
