/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <filesystem>
#include <fstream>

#include "blueprint.hpp"
#include "blueprint_exception.hpp"
#include "../thirdparty/cxxopts/include/cxxopts.hpp"


std::string read_file(const std::string& path) {
    if (!std::filesystem::exists(path))
        throw std::runtime_error("File " + path + " does not exist");
    std::ifstream in(path);
    in.seekg(0, std::ios::end);
    std::size_t size = in.tellg();
    in.seekg(0, std::ios::beg);
    std::string contents;
    contents.resize(size);
    in.read(contents.data(), size);
    return contents;
}

int main(int argc, char * argv[])
{
    std::string description = "Pizza Architect will create configs for Pizza Runner from json blueprint";
    cxxopts::Options options("Pizza Architect", std::move(description));
    options.add_options()
        ("b,blueprint", "Blueprint file path", cxxopts::value<std::string>())
        ("o,out", "Output directory path", cxxopts::value<std::string>())
        ("h,help", "Show this help");
    cxxopts::ParseResult result;
    try {
        result = options.parse(argc, argv);
    }
    catch (std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    if (result.count("help") > 0) {
        std::cout << options.help() << std::endl;
        return 0;
    }
    if (result.count("blueprint") == 0) {
        std::cout << "Error: missing blueprint option" << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    const auto& blueprint_path = result["blueprint"].as<std::string>();
    std::string out_path;
    if (result.count("out") == 0) {
        out_path = std::filesystem::path(blueprint_path).parent_path();
        std::cout << "No out path specified, using " << out_path << std::endl;
    }
    else {
        out_path = result["out"].as<std::string>();
    }
    try {
        pizza::blueprint blueprint;
        blueprint.parse(read_file(blueprint_path));
        blueprint.save(out_path);
    }
    catch (const std::exception& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
        return 1;
    }
    return 0;
}
