/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "owner.hpp"
#include <pizza-config/balancer.hpp>

namespace pizza {

class blueprint;

class ext_balancer : public balancer, public owner<blueprint> {
public:
    void set_broker_name(std::string broker_name) noexcept { broker_name_ = std::move(broker_name); }
    constexpr const std::string& broker_name() const noexcept { return broker_name_; }
private:
    std::string broker_name_;
};

};

