/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "blueprint.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>

#include <rapidjson/error/en.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

#include "balancer.hpp"
#include "blueprint_exception.hpp"
#include "broker.hpp"
#include "bus.hpp"
#include "service.hpp"

namespace {

using ex = pizza::blueprint_exception;

template <typename Json>
void ensure_has_member(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    if (!obj.HasMember(field))
        throw ex(ex::code::missing_member, field, std::move(stacktrace));
}

template <typename Json>
bool has_string_member(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    if (!obj.HasMember(field)) return false;
    if (!obj[field].IsString())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of string type",
                 std::move(stacktrace));
    return true;
}

template <typename Json>
bool has_array_member(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    if (!obj.HasMember(field)) return false;
    if (!obj[field].IsArray())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of array type",
                 std::move(stacktrace));
    return true;
}

template <typename Json>
bool has_uint64_member(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    if (!obj.HasMember(field)) return false;
    if (!obj[field].IsUint64())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of integer type",
                 std::move(stacktrace));
    return true;
}

template <typename Json>
void ensure_if_array_or_throw(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    ensure_has_member(field, obj, stacktrace);
    if (!obj[field].IsArray())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of array type",
                 std::move(stacktrace));
}

template <typename Json>
void ensure_if_string_or_throw(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    ensure_has_member(field, obj, stacktrace);
    if (!obj[field].IsString())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of string type",
                 std::move(stacktrace));
}

template <typename Json>
void ensure_if_object_or_throw(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    ensure_has_member(field, obj, stacktrace);
    if (!obj[field].IsObject())
        throw ex(ex::code::member_of_wrong_type,
                 std::string(field) + " must be of object type",
                 std::move(stacktrace));
}

template <typename Json>
std::string get_string_or_throw(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    ensure_has_member(field, obj, stacktrace);
    ensure_if_string_or_throw(field, obj, stacktrace);
    return obj[field].GetString();
}

template <typename Json>
std::string_view get_string_view_or_throw(const char field[], Json& obj, std::vector<std::string>& stacktrace) {
    ensure_has_member(field, obj, stacktrace);
    ensure_if_string_or_throw(field, obj, stacktrace);
    return std::string_view(obj[field].GetString(), obj[field].GetStringLength());
}

template <typename Derived>
Derived * safe_get(pizza::basic_config * cfg) {
    auto ptr = dynamic_cast<Derived*>(cfg);
    if (!ptr) throw std::runtime_error(__func__);
    return ptr;
}

std::unique_ptr<pizza::ext_broker> parse_broker(rapidjson::Value& val, std::vector<std::string>& stacktrace) {
    auto broker_ptr = std::make_unique<pizza::ext_broker>();
    broker_ptr->set_name(get_string_or_throw("name", val, stacktrace));
    stacktrace.pop_back();
    stacktrace.push_back("Broker (" + broker_ptr->name() + ")");
    broker_ptr->set_executable(get_string_or_throw("executable", val, stacktrace));
    broker_ptr->set_addr(get_string_or_throw("local_addr", val, stacktrace));
    broker_ptr->set_remote_addr(get_string_or_throw("remote_addr", val, stacktrace));
    broker_ptr->set_app(pizza::basic_config::application::broker);
    broker_ptr->set_type("runner_config");
    return broker_ptr;
}

std::unique_ptr<pizza::ext_bus> parse_bus(rapidjson::Value& val, std::vector<std::string>& stacktrace) {
    auto bus_ptr = std::make_unique<pizza::ext_bus>();
    bus_ptr->set_name(get_string_or_throw("name", val, stacktrace));
    stacktrace.pop_back();
    stacktrace.push_back("Bus (" + bus_ptr->name() + ")");
    bus_ptr->set_executable(get_string_or_throw("executable", val, stacktrace));

    bus_ptr->set_publishers_addr(get_string_or_throw("local_publishers_addr", val, stacktrace));
    bus_ptr->set_remote_publishers_addr(get_string_or_throw("remote_publishers_addr", val, stacktrace));

    bus_ptr->set_subscribers_addr(get_string_or_throw("local_subscribers_addr", val, stacktrace));
    bus_ptr->set_remote_subscribers_addr(get_string_or_throw("remote_subscribers_addr", val, stacktrace));

    bus_ptr->set_app(pizza::basic_config::application::bus);
    bus_ptr->set_type("runner_config");
    return bus_ptr;
}

std::string config_to_string(pizza::basic_config * cfg) {
    rapidjson::Document doc;
    doc.SetObject();
    cfg->to_json(doc);
    rapidjson::StringBuffer buf;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buf);
    doc.Accept(writer);
    return buf.GetString();
}

}

namespace pizza
{

void blueprint::parse(std::string str) {
    buf_ = std::move(str);
    json_stacktrace_.clear();
    json_stacktrace_.reserve(4);
    rapidjson::Document document;
    rapidjson::ParseResult result = document.Parse(buf_.data(), buf_.size());
    if (!result) {
        std::stringstream stream;
        stream << "failed parsing json: " << rapidjson::GetParseError_En(result.Code()) << " (" << result.Offset() << ")";
        throw std::runtime_error(stream.str());
    }
    auto type = get_string_view_or_throw("type", document, json_stacktrace_);
    if (type != "blueprint")
        throw ex(ex::code::undefined_format,
                 std::string(type.data(), type.length()),
                 std::move(json_stacktrace_));
    
    encryption_ = (document.HasMember("encryption") && document["encryption"].GetBool());
    get_broker_list(document);
    get_bus_list(document);
    get_services_list(document);
    normalize_addresses();
    normalize_keys();
}

void blueprint::save(const std::string& path)
{
    std::filesystem::path p = path;
    std::filesystem::create_directories(p);
    for (const auto& [name, cfg] : configs_) {
        std::filesystem::path curr_path = p / (name + "_config.json");
        std::string data = config_to_string(cfg.get());
        std::ofstream out(curr_path);
        out.write(data.data(), data.size());
        std::cout << curr_path.c_str() << std::endl;
    }
}

void blueprint::get_broker_list(rapidjson::Document& document)
{
    ensure_if_array_or_throw("broker_list", document, json_stacktrace_);

    json_stacktrace_.push_back("broker_list");
    std::size_t id = 0;
    for (auto& broker_obj : document["broker_list"].GetArray()) {
        json_stacktrace_.push_back("broker #" + std::to_string(id++));
        auto result = parse_broker(broker_obj, json_stacktrace_);
        result->set_owner(this);
        if (encryption_) {
            result->set_encryption(true);
            auto p = key_pair::create();
            result->keys().emplace("broker_private_key", p.private_key());
            result->keys().emplace("broker_public_key", p.public_key());
        }
        std::string name = result->name();
        configs_.emplace(std::move(name), std::move(result));
        json_stacktrace_.pop_back();
    }
    json_stacktrace_.pop_back();
}

void blueprint::get_bus_list(rapidjson::Document& document)
{
    ensure_if_array_or_throw("bus_list", document, json_stacktrace_);
    
    json_stacktrace_.push_back("bus_list");
    std::size_t id = 0;
    for (auto& bus_obj : document["bus_list"].GetArray()) {
        json_stacktrace_.push_back("bus #" + std::to_string(id++));
        auto result = parse_bus(bus_obj, json_stacktrace_);
        result->set_owner(this);
        if (encryption_) {
            result->set_encryption(true);
            auto sub_p = key_pair::create(),
                 pub_p = key_pair::create();
            
            result->keys().emplace("bus_sub_private_key", sub_p.private_key());
            result->keys().emplace("bus_sub_public_key", sub_p.public_key());

            result->keys().emplace("bus_pub_private_key", pub_p.private_key());
            result->keys().emplace("bus_pub_public_key", pub_p.public_key());
        }
        std::string name = result->name();
        configs_.emplace(std::move(name), std::move(result));
        json_stacktrace_.pop_back();
    }
    json_stacktrace_.pop_back();
}

void blueprint::get_services_list(rapidjson::Document& document)
{
    ensure_if_array_or_throw("services", document, json_stacktrace_);

    json_stacktrace_.push_back("services");
    std::size_t id = 0;
    for (auto& service_obj : document["services"].GetArray()) {
        auto service_ptr = std::make_unique<pizza::ext_service>();
        service_ptr->set_owner(this);
        service_ptr->set_type("runner_config");
        service_ptr->set_encryption(encryption_);
        json_stacktrace_.push_back("service #" + std::to_string(id++));
        service_ptr->set_name(get_string_or_throw("name", service_obj, json_stacktrace_));
        json_stacktrace_.pop_back();
        json_stacktrace_.push_back(service_ptr->name());
        service_ptr->set_executable(get_string_or_throw("executable", service_obj, json_stacktrace_));
        service_ptr->set_app(basic_config::application::service);
        service_ptr->set_broker_name(get_string_or_throw("broker", service_obj, json_stacktrace_));
        if (auto iter = configs_.find(service_ptr->broker_name());
            iter == configs_.end() || iter->second->app() != basic_config::application::broker)
            throw ex(ex::code::unknown_broker,
                     service_ptr->broker_name(),
                    std::move(json_stacktrace_));
        
        if (has_string_member("bus", service_obj, json_stacktrace_)) {
            std::string bus_name = service_obj["bus"].GetString();
            if (auto iter = configs_.find(bus_name);
                (iter == configs_.end() ||
                iter->second->app() != basic_config::application::bus)) {
                    throw ex(ex::code::unknown_bus,
                             bus_name,
                             std::move(json_stacktrace_));
            }
            service_ptr->set_bus_name(std::move(bus_name));
        }

        if (has_array_member("cl_args", service_obj, json_stacktrace_)) {
            json_stacktrace_.push_back("cl_args");
            for (auto& cl_arg : service_obj["cl_args"].GetArray()) {
                if (!cl_arg.IsString())
                    throw ex(ex::code::member_of_wrong_type,
                             "",
                             std::move(json_stacktrace_));
                service_ptr->cl_args().push_back(cl_arg.GetString());
            }
            json_stacktrace_.pop_back();
        }

        if (has_uint64_member("count", service_obj, json_stacktrace_)) {
            service_ptr->set_count(service_obj["count"].GetUint64());
        }
        if (has_uint64_member("threads", service_obj, json_stacktrace_)) {
            service_ptr->set_threads(service_obj["threads"].GetUint64());
        }

        if (service_ptr->count() > 1 || service_obj.HasMember("balancer")) {
            ensure_if_object_or_throw("balancer", service_obj, json_stacktrace_);
            auto balancer_obj = service_obj["balancer"].GetObject();
            json_stacktrace_.push_back("balancer");
            auto balancer_ptr = std::make_unique<ext_balancer>();
            balancer_ptr->set_owner(this);
            balancer_ptr->set_name(get_string_or_throw("name", balancer_obj, json_stacktrace_));
            service_ptr->set_balancer_name(balancer_ptr->name());
            balancer_ptr->set_addr(get_string_or_throw("addr", balancer_obj, json_stacktrace_));
            balancer_ptr->set_app(basic_config::application::balancer);
            balancer_ptr->set_executable(get_string_or_throw("executable", balancer_obj, json_stacktrace_));
            balancer_ptr->set_broker_name(service_ptr->broker_name());
            balancer_ptr->set_service_name(service_ptr->name());
            balancer_ptr->set_type("runner_config");
            if (service_ptr->encryption()) {
                balancer_ptr->set_encryption(true);
                auto key_p = key_pair::create();
                balancer_ptr->keys().emplace("balancer_private_key", key_p.private_key());
                balancer_ptr->keys().emplace("balancer_public_key", key_p.public_key());
            }
            std::string balancer_name = balancer_ptr->name();
            configs_.emplace(std::move(balancer_name), std::move(balancer_ptr));
            json_stacktrace_.pop_back();
        }

        std::string service_name = service_ptr->name();
        configs_.emplace(std::move(service_name), std::move(service_ptr));
        json_stacktrace_.pop_back();
    }
}

namespace
{

inline ext_balancer * get_balancer(basic_config * cfg) { return safe_get<ext_balancer>(cfg); }
inline ext_broker * get_broker(basic_config * cfg) { return safe_get<ext_broker>(cfg); }
inline ext_bus * get_bus(basic_config * cfg) { return safe_get<ext_bus>(cfg); }
inline ext_service * get_service(basic_config * cfg) { return safe_get<ext_service>(cfg); }

}

void blueprint::normalize_addresses()
{
    using app = basic_config::application;
    for (auto& [name, cfg] : configs_) {
        if (cfg->app() == app::balancer) {
            auto p = get_balancer(cfg.get());
            p->set_broker_addr(
                get_broker(configs_.at(p->broker_name()).get())
                ->remote_addr()
            );
        }
        else if (cfg->app() == app::service) {
            auto p = get_service(cfg.get());
            if (p->has_balancer_name()) {
                p->set_balancer_addr(
                    get_balancer(configs_.at(p->balancer_name()).get())
                    ->addr()
                );
            }
            if (p->has_bus_name()) {
                p->set_bus_subscribers_addr(
                    get_bus(configs_.at(p->bus_name()).get())
                    ->remote_subscribers_addr()
                );
                p->set_bus_publishers_addr(
                    get_bus(configs_.at(p->bus_name()).get())
                    ->remote_publishers_addr()
                );
            }
            p->set_broker_addr(
                get_broker(configs_.at(p->broker_name()).get())
                ->remote_addr()
            );
        }
    }
}

void blueprint::normalize_keys()
{
    using app = basic_config::application;
    for (auto& [name, cfg] : configs_) {
        if (cfg->encryption() && cfg->app() == app::service) {
            auto p = get_service(cfg.get());
            if (p->has_balancer()) {
                auto balancer = get_balancer(configs_.at(p->balancer_name()).get());
                p->keys().emplace("balancer_public_key",
                    balancer->keys().at("balancer_public_key"));
            }
            if (p->has_bus()) {
                auto bus = get_bus(configs_.at(p->bus_name()).get());
                p->keys().emplace("bus_sub_public_key",
                    bus->keys().at("bus_sub_public_key"));
                p->keys().emplace("bus_pub_public_key",
                    bus->keys().at("bus_pub_public_key"));
            }
            auto broker = get_broker(configs_.at(p->broker_name()).get());
            p->keys().emplace("broker_public_key",
                broker->keys().at("broker_public_key"));
        }
    }

    for (auto& [name, cfg] : configs_) {
        if (cfg->encryption() && cfg->app() == app::balancer) {
            auto p = get_balancer(cfg.get());
            auto broker = get_broker(configs_.at(p->broker_name()).get());
            p->keys().emplace("broker_public_key",
                broker->keys().at("broker_public_key"));
        }
    }
}

} // namespace pizza
