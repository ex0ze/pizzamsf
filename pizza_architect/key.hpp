/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <array>
#include <string>

namespace pizza
{

class key_pair {
public:
    static constexpr std::size_t key_len = 41;
    using key_type = std::array<char, key_len>;

    static key_pair create();

    std::string public_key() const noexcept { return std::string(public_key_.begin(), public_key_.end()); }
    std::string private_key() const noexcept { return std::string(private_key_.begin(), private_key_.end()); }

private:
    key_pair() = default;

    key_type public_key_;
    key_type private_key_;
};

} // namespace pizza
