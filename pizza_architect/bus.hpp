/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "owner.hpp"
#include <pizza-config/bus.hpp>

namespace pizza {

class blueprint;

class ext_bus : public bus, public owner<blueprint> {
public:
    void set_remote_subscribers_addr(std::string remote_subscribers_addr) noexcept {
        remote_subscribers_addr_ = std::move(remote_subscribers_addr); 
    }
    constexpr const std::string& remote_subscribers_addr() const noexcept { return remote_subscribers_addr_; }

    void set_remote_publishers_addr(std::string remote_publishers_addr) noexcept {
        remote_publishers_addr_ = std::move(remote_publishers_addr); 
    }
    constexpr const std::string& remote_publishers_addr() const noexcept { return remote_publishers_addr_; }
    
private:
    std::string remote_subscribers_addr_;
    std::string remote_publishers_addr_;
};

};

