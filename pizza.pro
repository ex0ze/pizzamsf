TEMPLATE = subdirs

SUBDIRS = \
pizza-broker \
    pizza-microservice-examples \
pizza-proxy-balancer \
pizza-microservice \
pizza-runner \
pizza-runner-daemon \
pizza-runner-cli

pizza-microservice-examples.depends = pizza-microservice
pizza-runner-daemon.depends = pizza-runner
pizza-runner-cli.depends = pizza-runner
