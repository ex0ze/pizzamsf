TEMPLATE = lib
CONFIG += console c++17 thread
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../build/
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
HEADERS += \
    include/pizza-runner/ConfigLoader.h \
    include/pizza-runner/Exception.hpp \
    include/pizza-runner/MessageHandlersRunner.h \
    include/pizza-runner/Runner.h \
    include/pizza-runner/RunnerConfig.h \
    include/pizza-runner/RunnerConfigFormat.hpp \
    include/pizza-runner/ServerMessageFormat.hpp \
    include/pizza-runner/entry/Balancer.h \
    include/pizza-runner/entry/Broker.h \
    include/pizza-runner/entry/IEntry.h \
    include/pizza-runner/entry/Service.h \
    include/pizza-runner/helpers.h \
    include/pizza-runner/json/PizzaRunnerJsonHelpers.h \
    include/pizza-runner/local_server/LocalServer.h \
    include/pizza-runner/message/ErrorReplyMessage.h \
    include/pizza-runner/message/IMessage.h \
    include/pizza-runner/message/IReplyMessage.h \
    include/pizza-runner/message/IRequestMessage.h \
    include/pizza-runner/message/JSONMessageCreator.h \
    include/pizza-runner/message/LoadMessage.h \
    include/pizza-runner/message/ReloadMessage.h \
    include/pizza-runner/message/ReplyRegularStatisticsMessage.h \
    include/pizza-runner/message/ReplyStatisticsMessage.h \
    include/pizza-runner/message/RequestMessageWithName.h \
    include/pizza-runner/message/RequestMessageWithPath.h \
    include/pizza-runner/message/RequestRegularStatisticsMessage.h \
    include/pizza-runner/message/RestartMessage.h \
    include/pizza-runner/message/RunMessage.h \
    include/pizza-runner/message/StopMessage.h \
    include/pizza-runner/message/UnloadMessage.h \
    include/pizza-runner/stats/IStatisticsCollector.h \
    include/pizza-runner/stats/RegularStatisticsCollector.h \
    include/pizza-runner/stats/RegularStats.h

SOURCES += \
    src/ConfigLoader.cpp \
    src/MessageHandlersRunner.cpp \
    src/Runner.cpp \
    src/RunnerConfig.cpp \
    src/entry/Balancer.cpp \
    src/entry/Broker.cpp \
    src/entry/IEntry.cpp \
    src/entry/Service.cpp \
    src/helpers.cpp \
    src/local_server/LocalServer.cpp \
    src/message/ErrorReplyMessage.cpp \
    src/message/IMessage.cpp \
    src/message/IReplyMessage.cpp \
    src/message/IRequestMessage.cpp \
    src/message/JSONMessageCreator.cpp \
    src/message/LoadMessage.cpp \
    src/message/ReloadMessage.cpp \
    src/message/ReplyRegularStatisticsMessage.cpp \
    src/message/ReplyStatisticsMessage.cpp \
    src/message/RequestMessageWithName.cpp \
    src/message/RequestMessageWithPath.cpp \
    src/message/RequestRegularStatisticsMessage.cpp \
    src/message/RestartMessage.cpp \
    src/message/RunMessage.cpp \
    src/message/StopMessage.cpp \
    src/message/UnloadMessage.cpp \
    src/stats/IStatisticsCollector.cpp \
    src/stats/RegularStatisticsCollector.cpp \
    src/stats/RegularStats.cpp
