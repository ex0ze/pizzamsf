#ifndef PIZZARUNNEREXCEPTION_HPP
#define PIZZARUNNEREXCEPTION_HPP
#include <string>
#include <exception>
#include <ostream>

namespace pizza::runner::exception {

namespace common_ex {

constexpr char kMissingField[] = "Missing field";
constexpr char kWrongArgument[] = "Wrong argument";
constexpr char kMissingBalancer[] = "Missing balancer for microservice count > 1";
constexpr char kNull[] = "";
}

struct Exception : public std::exception
{
    friend std::ostream& operator<<(std::ostream& os, const Exception& e)
    {
        os << "Module: " << e.p_module << '\n';
        os << "What: " << e.p_what << '\n';
        os << "Detail: " << e.p_detail << std::endl;
        return os;
    }
    Exception(const char * module, const char * what, const char * detail = common_ex::kNull) noexcept
        : p_module(module), p_what(what), p_detail(detail) {}
    // exception interface
public:
    virtual const char * what() const noexcept override
    {
        return p_what;
    }
    const char * module() const noexcept
    {
        return p_module;
    }
    const char * detail() const noexcept
    {
        return p_detail;
    }
private:
    const char * p_module;
    const char * p_what;
    const char * p_detail;
};

}


#endif // PIZZARUNNEREXCEPTION_HPP
