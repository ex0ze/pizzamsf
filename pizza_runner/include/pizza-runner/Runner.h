#ifndef PIZZARUNNER_H
#define PIZZARUNNER_H

#include "RunnerConfig.h"
#include <unordered_map>
#include <string_view>


namespace pizza::runner {

namespace statistics
{
    class IStatisticsCollector;
}

class Runner
{
public:
    bool addConfig(config::Config conf);
    bool removeConfig(const std::string& confName);
    bool runConfig(const std::string& confName);
    bool stopConfig(const std::string& confName);
    void acceptStatisticsCollector(statistics::IStatisticsCollector&) const;
    const std::unordered_map<std::string, config::Config>& getConfigs() const noexcept;
    config::Config * config(const std::string& confName) noexcept;
private:
    std::unordered_map<std::string, config::Config> m_configHash;
};

}
#endif // PIZZARUNNER_H
