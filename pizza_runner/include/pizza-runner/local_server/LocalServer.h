#ifndef LOCALSERVER_H
#define LOCALSERVER_H

#include "../../../../thirdparty/cppzmq/zmq.hpp"
#include "../../../../thirdparty/cppzmq/zmq_addon.hpp"
#include <string>
#include <optional>

namespace pizza::runner::srv {

class LocalServer
{
public:
    LocalServer(const std::string& bindAddr);
    std::string recv();
    void reply(const std::string& msg);
private:
    zmq::context_t m_context;
    zmq::socket_t m_socket;
};

}

#endif // LOCALSERVER_H
