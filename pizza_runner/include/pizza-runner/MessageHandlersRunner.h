#ifndef MESSAGEHANDLERS_H
#define MESSAGEHANDLERS_H

#include <rapidjson/document.h>
#include <unordered_map>
#include <string_view>
#include <functional>
#include "helpers.h"
#include "message/IMessage.h"

namespace pizza::runner::srv::handlers {

class HandlersRunner
{
public:
    HandlersRunner() = default;
    //! return - result of processing
    //! arg - const ref to input message
    using handler_t = std::function<message::IMessage::Ptr(message::IMessage::Ptr)>;
    bool registerHandler(message::IMessage::MessagePacket packet, message::IMessage::MessageAction act, handler_t handler);
    std::string processMessage(std::string_view json);
private:
    std::unordered_map<
        std::pair<message::IMessage::MessagePacket,
            message::IMessage::MessageAction>, handler_t,
        pizza::helpers::CantorHash> m_handlers;
};

}

#endif // MESSAGEHANDLERS_H
