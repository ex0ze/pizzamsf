#ifndef IREQUESTMESSAGE_H
#define IREQUESTMESSAGE_H

#include "IMessage.h"

namespace pizza::runner::message {
//! Empty class, created for separation request - reply messages
class IRequestMessage : public IMessage
{
public:
    using Base = IMessage;
    IRequestMessage(IMessage::MessageAction act);

    // IMessage interface
public:
    virtual std::string toJSON() override;

protected:
    virtual bool fromJSON(const rapidjson::Document & doc) override;
    virtual void writeFields() override;
    virtual bool readFields(const rapidjson::Document & doc) override;
};

}
#endif // IREQUESTMESSAGE_H
