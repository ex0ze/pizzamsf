#ifndef REQUESTMESSAGEWITHPATH_H
#define REQUESTMESSAGEWITHPATH_H

#include "IRequestMessage.h"
namespace pizza::runner::message {

class RequestMessageWithPath : public IRequestMessage
{
public:
    using Base = IRequestMessage;
    RequestMessageWithPath(IMessage::MessageAction act) : IRequestMessage(act) {}
    void setPath(std::string path);
    const std::string& path() const noexcept;

private:
    std::string m_path;

    // IMessage interface
public:
    virtual std::string toJSON() override;

protected:
    virtual bool fromJSON(const rapidjson::Document & doc) override;
    virtual void writeFields() override;
    virtual bool readFields(const rapidjson::Document & doc) override;
};

}

#endif // REQUESTMESSAGEWITHPATH_H
