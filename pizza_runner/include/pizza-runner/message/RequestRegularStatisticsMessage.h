#ifndef REQUESTREGULARSTATISTICSMESSAGE_H
#define REQUESTREGULARSTATISTICSMESSAGE_H

#include "IRequestMessage.h"

namespace pizza::runner::message
{

class RequestRegularStatisticsMessage final : public IRequestMessage
{
public:
    RequestRegularStatisticsMessage();
    static Ptr create();
};

}

#endif // REQUESTREGULARSTATISTICSMESSAGE_H
