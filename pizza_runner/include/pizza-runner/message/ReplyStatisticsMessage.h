#ifndef REPLYSTATISTICSMESSAGE_H
#define REPLYSTATISTICSMESSAGE_H

#include "IReplyMessage.h"
#include "../stats/IStatisticsCollector.h"

namespace pizza::runner::message
{

class ReplyStatisticsMessage : public IReplyMessage
{
public:
    using Base = IReplyMessage;
    ReplyStatisticsMessage(MessageAction act) : IReplyMessage(act) {}
    ReplyStatisticsMessage& setCollector(std::unique_ptr<statistics::IStatisticsCollector> c) noexcept;
    statistics::IStatisticsCollector* collector() noexcept;
    std::unique_ptr<statistics::IStatisticsCollector> consumeCollector() noexcept;
private:

    // IMessage interface
public:
    virtual std::string toJSON() override;

protected:
    virtual void writeFields() override;
    std::unique_ptr<statistics::IStatisticsCollector> p_statsCollector;
};

}

#endif // REPLYSTATISTICSMESSAGE_H
