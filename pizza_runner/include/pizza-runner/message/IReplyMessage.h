#ifndef IREPLYMESSAGE_H
#define IREPLYMESSAGE_H

#include "IMessage.h"

namespace pizza::runner::message
{

class IReplyMessage : public IMessage
{
public:
    using Base = IMessage;
    enum class MessageStatus : int32_t
    {
        Success,
        Failed
    };
    virtual ~IReplyMessage() = default;
    IReplyMessage(IMessage::MessageAction act);
    const std::string& what() const noexcept;
    IReplyMessage& setWhat(std::string what);
    MessageStatus status() const noexcept;
    IReplyMessage& setStatus(MessageStatus status) noexcept;
    static std::optional<std::string_view> recognizeStatus(MessageStatus status) noexcept;
    static std::optional<MessageStatus> recognizeStatus(std::string_view status) noexcept;
private:
    MessageStatus m_status {MessageStatus::Failed};
    std::string m_what;

    static const std::map<MessageStatus, std::string_view> statusViewTable;

    // IMessage interface
public:
    virtual std::string toJSON() override;
protected:
    virtual bool fromJSON(const rapidjson::Document &) override;
    virtual void writeFields() override;
    virtual bool readFields(const rapidjson::Document&) override;

};

}

#endif // IREPLYMESSAGE_H
