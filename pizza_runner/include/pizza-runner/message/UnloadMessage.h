#ifndef UNLOADMESSAGE_H
#define UNLOADMESSAGE_H


#include "RequestMessageWithName.h"
#include "IReplyMessage.h"

namespace pizza::runner::message
{

class RequestUnloadMessage final : public RequestMessageWithName
{
public:
    RequestUnloadMessage();
    static Ptr create();
};

class ReplyUnloadMessage final : public IReplyMessage
{
public:
    ReplyUnloadMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}

#endif // UNLOADMESSAGE_H
