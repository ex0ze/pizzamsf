#ifndef REPLYREGULARSTATISTICSMESSAGE_H
#define REPLYREGULARSTATISTICSMESSAGE_H

#include "ReplyStatisticsMessage.h"

namespace pizza::runner::message {

class ReplyRegularStatisticsMessage final : public ReplyStatisticsMessage
{
public:
    using Base = ReplyStatisticsMessage;
    ReplyRegularStatisticsMessage();
    static Ptr create();

    // IMessage interface
protected:
    virtual bool fromJSON(const rapidjson::Document &) override;
    virtual bool readFields(const rapidjson::Document &) override;
};

}
#endif // REPLYREGULARSTATISTICSMESSAGE_H
