#ifndef LOADMESSAGE_H
#define LOADMESSAGE_H

#include "RequestMessageWithPath.h"
#include "IReplyMessage.h"

namespace pizza::runner::message
{

class RequestLoadMessage final : public RequestMessageWithPath
{
public:
    RequestLoadMessage();
    static Ptr create();
};

class ReplyLoadMessage final : public IReplyMessage
{
public:
    ReplyLoadMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}
#endif // LOADMESSAGE_H
