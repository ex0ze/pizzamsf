#ifndef RELOADMESSAGE_H
#define RELOADMESSAGE_H


#include "RequestMessageWithName.h"
#include "IReplyMessage.h"

namespace pizza::runner::message
{

class RequestReloadMessage final : public RequestMessageWithName
{
public:
    RequestReloadMessage();
    static Ptr create();
};

class ReplyReloadMessage final : public IReplyMessage
{
public:
    ReplyReloadMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}

#endif // RELOADMESSAGE_H
