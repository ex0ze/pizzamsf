#ifndef REQUESTMESSAGEWITHNAME_H
#define REQUESTMESSAGEWITHNAME_H

#include "IRequestMessage.h"

namespace pizza::runner::message {

class RequestMessageWithName : public IRequestMessage
{
public:
    using Base = IRequestMessage;
    RequestMessageWithName(IMessage::MessageAction act) : IRequestMessage(act) {}
    void setName(std::string name);
    const std::string& name() const noexcept;

private:
    std::string m_name;

    // IMessage interface
public:
    virtual std::string toJSON() override;

protected:
    virtual bool fromJSON(const rapidjson::Document & doc) override;
    virtual void writeFields() override;
    virtual bool readFields(const rapidjson::Document & doc) override;
};

}

#endif // REQUESTMESSAGEWITHNAME_H
