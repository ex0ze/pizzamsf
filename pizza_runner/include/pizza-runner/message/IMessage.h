#ifndef IMESSAGE_H
#define IMESSAGE_H

#include <string_view>
#include <memory>
#include <optional>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <map>
#include "../ServerMessageFormat.hpp"
#include "../json/PizzaRunnerJsonHelpers.h"

namespace pizza::runner::message
{

class IMessage
{
    friend class JSONMessageCreator;
public:
    using Ptr = std::unique_ptr<IMessage>;
    using CreateFn = Ptr(*)();
    enum class MessagePacket : int32_t
    {
        Request = 0,
        Reply
    };
    enum class MessageAction : int32_t
    {
        Unknown = 0,
        Load,
        Unload,
        Run,
        Stop,
        Restart,
        Reload,
        CollectRegularStatistics,
        CollectVerboseStatistics
    };
    IMessage(MessagePacket packet, MessageAction action);
    // in derived classes first call Derived::writeFields() then Base::toJson()
    virtual std::string toJSON();
    virtual ~IMessage() = default;
    MessagePacket packet() const noexcept;
    MessageAction action() const noexcept;
    static std::optional<MessageAction> recognizeAction(std::string_view action) noexcept;
    static std::optional<std::string_view> recognizeAction(MessageAction action) noexcept;
    static std::optional<MessagePacket> recognizePacket(std::string_view packet) noexcept;
    static std::optional<std::string_view> recognizePacket(MessagePacket packet) noexcept;
    rapidjson::Document * getDocument() noexcept;

private:
    MessagePacket m_packet;
    MessageAction m_action;

    //! i am aware of boost::bimap, it's too overkill for this simple task
    static const std::map<MessageAction, std::string_view> actionViewTable;
    static const std::map<MessagePacket, std::string_view> packetViewTable;

protected:
    std::unique_ptr<rapidjson::Document> p_doc;
    virtual bool fromJSON(const rapidjson::Document&);
    virtual void writeFields();
    virtual bool readFields(const rapidjson::Document&);
};

}
#endif // IMESSAGE_H
