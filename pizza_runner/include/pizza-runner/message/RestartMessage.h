#ifndef RESTARTMESSAGE_H
#define RESTARTMESSAGE_H


#include "RequestMessageWithName.h"
#include "IReplyMessage.h"

namespace pizza::runner::message
{

class RequestRestartMessage final : public RequestMessageWithName
{
public:
    RequestRestartMessage();
    static Ptr create();
};

class ReplyRestartMessage final : public IReplyMessage
{
public:
    ReplyRestartMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}

#endif // RESTARTMESSAGE_H
