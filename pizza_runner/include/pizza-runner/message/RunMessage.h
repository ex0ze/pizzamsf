#ifndef RUNMESSAGE_H
#define RUNMESSAGE_H


#include "RequestMessageWithName.h"
#include "IReplyMessage.h"
#include "JSONMessageCreator.h"

namespace pizza::runner::message
{

class RequestRunMessage final : public RequestMessageWithName
{
public:
    RequestRunMessage();
    static Ptr create();
};

class ReplyRunMessage final : public IReplyMessage
{
public:
    ReplyRunMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}

#endif // RUNMESSAGE_H
