#ifndef ERRORREPLYMESSAGE_H
#define ERRORREPLYMESSAGE_H

#include "IReplyMessage.h"

namespace pizza::runner::message
{

class ErrorReplyMessage final : public IReplyMessage
{
public:
    ErrorReplyMessage();
    static Ptr create();
};

}
#endif // ERRORREPLYMESSAGE_H
