#ifndef STOPMESSAGE_H
#define STOPMESSAGE_H


#include "RequestMessageWithName.h"
#include "IReplyMessage.h"

namespace pizza::runner::message
{

class RequestStopMessage final : public RequestMessageWithName
{
public:
    RequestStopMessage();
    static Ptr create();
};

class ReplyStopMessage final : public IReplyMessage
{
public:
    ReplyStopMessage(IReplyMessage::MessageStatus status = IReplyMessage::MessageStatus::Failed, std::string what = {});
    static Ptr create();
};

}

#endif // STOPMESSAGE_H
