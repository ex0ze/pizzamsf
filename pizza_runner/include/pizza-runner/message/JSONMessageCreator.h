#ifndef JSONMESSAGECREATOR_H
#define JSONMESSAGECREATOR_H

#include "IMessage.h"
#include <string_view>
#include <unordered_map>
#include "../helpers.h"

namespace pizza::runner::message {

class JSONMessageCreator
{
public:
    using MessageIdentity = std::pair<IMessage::MessagePacket, IMessage::MessageAction>;
    static JSONMessageCreator& instance()
    {
        static JSONMessageCreator creator;
        return creator;
    }
    IMessage::Ptr fromJSON(std::string_view json) const;
    std::unordered_map<MessageIdentity, IMessage::CreateFn,
        pizza::helpers::CantorHash>& map()
    {
        return m_createMessageTable;
    }
private:
    JSONMessageCreator();
    void initializeLookupTable();
    std::optional<IMessage::CreateFn> findFunction(IMessage::MessagePacket, IMessage::MessageAction) const noexcept;
    std::unordered_map<MessageIdentity, IMessage::CreateFn,
        pizza::helpers::CantorHash> m_createMessageTable;
};

}

#endif // JSONMESSAGECREATOR_H
