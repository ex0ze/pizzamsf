#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include "entry/IEntry.h"
#include "helpers.h"
#include <rapidjson/document.h>

namespace pizza::runner::config
{


class ConfigLoader
{
public:
    DEFAULT_COPY_MOVE(ConfigLoader)
    std::vector<entry::IEntry::Ptr>&& consumeResult() noexcept;
    std::string&& consumeName() noexcept;
    bool load(std::string_view path);
private:
    std::vector<entry::IEntry::Ptr> m_result;
    std::string m_configName;

    bool loadBrokers(rapidjson::Document&);
    bool loadServicesAndBalancers(rapidjson::Document&);
};

}

#endif // CONFIGLOADER_H
