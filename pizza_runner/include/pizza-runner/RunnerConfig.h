#ifndef PIZZARUNNERCONFIG_H
#define PIZZARUNNERCONFIG_H
#include <string_view>
#include <filesystem>
#include <variant>
#include <chrono>
#include <vector>

#include "ConfigLoader.h"
#include "helpers.h"

namespace pizza::runner::statistics
{
class RegularStatisticsCollector;
}

namespace pizza::runner::config {

class Config
{
    friend class statistics::RegularStatisticsCollector;
public:
    bool load(std::string_view path);
    bool run();
    void terminate();
    static std::optional<Config> fromFile(std::string_view path);
    std::string configName() const;
    int64_t launchedAt() const noexcept;
    int64_t currentRunTime() const noexcept;
    std::string path() const;
private:
    std::filesystem::path m_configPath;
    std::vector<entry::IEntry::Ptr> m_entries;
    std::string m_configName;
    ConfigLoader m_loader;
    int64_t m_launchedAt {0};
    bool m_launching;
    inline void setLaunchedAt();
};

}


#endif // PIZZARUNNERCONFIG_H
