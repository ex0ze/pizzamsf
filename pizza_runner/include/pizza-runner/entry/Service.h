#ifndef SERVICE_H
#define SERVICE_H

#include "IEntry.h"

namespace pizza::runner::config {
class ConfigLoader;
}

namespace pizza::runner::entry {

class Service : public IEntry
{
public:
    friend class pizza::runner::config::ConfigLoader;
    Service() : IEntry(IEntry::Type::Service) {}
    std::string name() const;
    void setName(std::string name);

    std::string executable() const;
    void setExecutable(std::string executable);

    std::string brokerAddr() const;
    void setBrokerAddr(std::string brokerAddr);

    int count() const;
    void setCount(int count);

    std::string balancerAddr() const;
    void setBalancerAddr(std::string balancerAddr);

    const std::map<std::string, std::string>& clArgs() const;
    void setClArgs(std::map<std::string, std::string> clArgs);

    virtual std::string executablePath() const override;
    //! --broker-addr <broker> --balancer-addr <balancer> --service-name <name> <cl args>
    virtual std::string commandLineArgs() const override;

private:
    std::string m_name;
    std::string m_executable;
    std::string m_brokerAddr;
    std::string m_balancerAddr;
    int m_count;
    std::map<std::string, std::string> m_clArgs;

};

}
#endif // SERVICE_H
