#ifndef BALANCER_H
#define BALANCER_H

#include "IEntry.h"

namespace pizza::runner::config {
class ConfigLoader;
}

namespace pizza::runner::entry {

class Balancer : public IEntry
{
public:
    friend class pizza::runner::config::ConfigLoader;
    Balancer()
        : IEntry(IEntry::Type::Balancer)
    {}
    virtual std::string executablePath() const override;
    virtual std::string commandLineArgs() const override;

    std::string name() const;
    void setName(std::string name);

    std::string executable() const;
    void setExecutable(std::string executable);

    std::string frontendAddr() const;
    void setFrontendAddr(std::string frontendAddr);

    std::string backendAddr() const;
    void setBackendAddr(std::string backendAddr);

private:
    std::string m_name;
    std::string m_executable;
    std::string m_frontendAddr;
    std::string m_backendAddr;

    // IEntry interface

};

}
#endif // BALANCER_H
