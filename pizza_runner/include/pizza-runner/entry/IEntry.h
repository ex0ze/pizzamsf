#ifndef IENTRY_H
#define IENTRY_H
#include <memory>
#include <string>
#include <sstream>
#include <boost/process/child.hpp>
namespace pizza::runner::entry
{

class IEntry
{
public:
    using Ptr = std::unique_ptr<IEntry>;
    enum class Type
    {
        Undefined,
        Broker,
        Balancer,
        Service
    };
    virtual ~IEntry() = default;
    inline Type type() const noexcept { return m_type; }

    virtual bool run(std::chrono::milliseconds waitDuration = std::chrono::milliseconds(200));
    virtual bool terminate();
    virtual std::string executablePath() const { return std::string(); }
    virtual std::string commandLineArgs() const { return std::string(); }
    int64_t launchedAt() const noexcept;
    int64_t currentRunTime() const noexcept;
    virtual bool running() const;
private:
    Type m_type;
    int64_t m_launchedAt = -1;
    void setLaunchedAt();
protected:
    explicit IEntry(Type t = Type::Undefined) : m_type(t) {}
    std::shared_ptr<boost::process::child> p_process;
};

}


#endif // IENTRY_H
