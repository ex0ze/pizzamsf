#ifndef BROKER_H
#define BROKER_H

#include "IEntry.h"

namespace pizza::runner::config {
class ConfigLoader;
}

namespace pizza::runner::entry {

class Broker : public IEntry
{
public:
    friend class pizza::runner::config::ConfigLoader;
    Broker()
        : IEntry(IEntry::Type::Broker)
    {}
    std::string executable() const;
    void setExecutable(std::string executable);

    std::string addr() const;
    void setAddr(std::string addr);

    int ioThreads() const;
    void setIoThreads(int ioThreads);
    //! --addr <broker addr> --io-threads <io threads>
    virtual std::string commandLineArgs() const override;
    virtual std::string executablePath() const override;

private:
    std::string m_executable;
    std::string m_addr;
    int m_ioThreads;

};

}
#endif // BROKER_H
