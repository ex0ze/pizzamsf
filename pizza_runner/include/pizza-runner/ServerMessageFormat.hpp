#ifndef SERVERMESSAGEFORMAT_HPP
#define SERVERMESSAGEFORMAT_HPP

namespace pizza::runner::srv::format
{

inline constexpr char kDefaultDaemonAddr[] = "ipc:///tmp/pizzarunnerdaemon";

namespace error_templates
{
    inline constexpr char kMalformedMessage[] = R"({
        "packet": "reply",
        "status": "malformed"})";

    inline constexpr char kUnsupportedMessage[] = R"({
        "packet": "reply",
        "status": "unsupported"})";

    inline constexpr char kBadArgument[] = R"({
        "packet": "reply",
        "status": "bad_argument"})";

}

inline constexpr char kPacket[] = "packet";
namespace packet
{
    inline constexpr char kRequest[] = "request";
    inline constexpr char kReply[] = "reply";
}


inline constexpr char kAction[] = "action";
namespace action
{
    //! for errors
    inline constexpr char kUnknownAction[] = "unknown";
    //! load JSON config
    inline constexpr char kLoad[] = "load";
    //! //! unload existing config from operating memory
    inline constexpr char kUnload[] = "unload";
    //! run existing JSON config
    inline constexpr char kRun[] = "run";
    //! stop existing JSON config
    inline constexpr char kStop[] = "stop";

    //! restart existing JSON config
    inline constexpr char kRestart[] = "restart";
    //! reload existing JSON config from path (unload + load)
    inline constexpr char kReload[] = "reload";

    inline constexpr char kCollectRegularStatistics[] = "regular_statistics";
    inline constexpr char kCollectVerboseStatistics[] = "verbose_statistics";
}


namespace request
{

    inline constexpr char kConfigName[] = "config_name";
    inline constexpr char kConfigPath[] = "config_path";
}

namespace reply
{
    inline constexpr char kStatus[] = "status";
    inline constexpr char kReason[] = "what";    

    namespace status {
        inline constexpr char kSuccess[] = "success";
        inline constexpr char kFailed[] = "failed";
    }
    namespace reason {
        inline constexpr char kFileNotFound[] = "file not found";
    }

    namespace statistics
    {

        namespace regular
        {
            inline constexpr char kName[] = "config name";
            inline constexpr char kRunningBrokers[] = "running brokers";
            inline constexpr char kNumBrokers[] = "num brokers";
            inline constexpr char kRunningBalancers[] = "running balancers";
            inline constexpr char kNumBalancers[] = "num balancers";
            inline constexpr char kRunningServices[] = "running services";
            inline constexpr char kNumServices[] = "num services";
            inline constexpr char kRunningTime[] = "running time";
        }
    }
}

}

#endif // SERVERMESSAGEFORMAT_HPP
