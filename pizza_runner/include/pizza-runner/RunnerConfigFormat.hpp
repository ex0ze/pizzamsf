#ifndef PIZZARUNNERCONFIGFORMAT_HPP
#define PIZZARUNNERCONFIGFORMAT_HPP

namespace pizza::runner::format
{
inline constexpr char kBrokers[] = "brokers";
inline constexpr char kServices[] = "services";
inline constexpr char kConfig[] = "config";

namespace brokers
{
inline constexpr char kExecutable[] = "executable";
inline constexpr char kAddr[] = "addr";
inline constexpr char kIoThreads[] = "io_threads";
}

namespace services
{
inline constexpr char kName[] = "name";
inline constexpr char kExecutable[] = "executable";
inline constexpr char kCount[] = "count";
inline constexpr char kBrokerAddr[] = "broker_addr";
inline constexpr char kBalancer[] = "balancer";
inline constexpr char kClArgs[] = "cl_args";

}

namespace balancer
{
inline constexpr char kAuto[] = "auto";
inline constexpr char kAddr[] = "addr";
inline constexpr char kExecutable[] = "executable";
}

}

#endif // PIZZARUNNERCONFIGFORMAT_HPP
