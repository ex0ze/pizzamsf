#ifndef HELPERS_H
#define HELPERS_H

#define DEFAULT_COPY_MOVE(cl)               \
    cl() = default;                         \
    cl(const cl&) = default;                \
    cl(cl&&) noexcept = default;            \
    cl& operator=(const cl&) = default;     \
    cl& operator=(cl&&) noexcept = default;

#define ONLY_MOVE_CTOR(cl)                  \
    cl() = default;                         \
    cl(const cl&) = delete;                 \
    cl& operator=(const cl&) = delete;      \
    cl(cl&&) noexcept = default;            \
    cl& operator=(cl&&) noexcept = default  \

#include <fstream>
#include <filesystem>
#include <string>
#include <optional>
#include <string_view>

namespace pizza::helpers
{

std::optional<std::string> readFile(std::string_view path);

template <class Object>
class SingleInstanceOf
{
public:
    template <typename ... Args>
    static Object& get(Args && ... args)
    {
        static Object instance(std::forward<Args>(args)...);
        return instance;
    }
};

int64_t currentRelativeClock();

int64_t currentAbsoluteClock();


struct CantorHash
{
    template <typename First, typename Second>
    std::size_t operator()(const std::pair<First, Second>& p) const
    {
        auto first = static_cast<std::underlying_type_t<First>>(p.first);
        auto second = static_cast<std::underlying_type_t<Second>>(p.second);
        return (first + second) * (first + second + 1) / 2 + second;
    }
};

}

#endif // HELPERS_H
