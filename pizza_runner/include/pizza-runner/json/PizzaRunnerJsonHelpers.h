#ifndef PIZZARUNNERJSONHELPERS_H
#define PIZZARUNNERJSONHELPERS_H

namespace pizza::runner::helpers::json {

template <typename JsonObject, typename ... Members>
bool hasAllMembers(JsonObject const & jsonObj, const Members & ... members)
{
    return (jsonObj.HasMember(members) && ... && true);
}

template<typename JsonObject, typename ... Members>
bool isAllMembersString(JsonObject const & jsonObj, const Members & ... members)
{
    return (jsonObj[members].IsString() && ... && true);
}

template<typename JsonObject, typename ... Members>
bool isAllMembersInt(JsonObject const & jsonObj, const Members & ... members)
{
    return (jsonObj[members].IsInt() && ... && true);
}

template<typename JsonObject, typename ... Members>
bool isAllMembersInt64(JsonObject const & jsonObj, const Members & ... members)
{
    return (jsonObj[members].IsInt64() && ... && true);
}




template <typename JsonObject, typename LhsRef>
bool maybeGetString(JsonObject const & jsonObj, LhsRef& lhs, const char member[])
{
    if (!jsonObj.HasMember(member) || !jsonObj[member].IsString())
        return false;
    else
    {
        lhs = jsonObj[member].GetString();
        return true;
    }
}

template <typename JsonObject, typename LhsRef>
bool maybeGetStringView(JsonObject const & jsonObj, LhsRef& lhs, const char member[])
{
    if (!jsonObj.HasMember(member) || !jsonObj[member].IsString())
        return false;
    else
    {
        lhs = LhsRef(jsonObj[member].GetString(), jsonObj[member].GetStringLength());
        return true;
    }
}

template <typename JsonObject, typename LhsRef>
bool maybeGetInt(JsonObject const & jsonObj, LhsRef& lhs, const char member[])
{
    if (!jsonObj.HasMember(member) || !jsonObj[member].IsInt())
        return false;
    else
    {
        lhs = jsonObj[member].GetInt();
        return true;
    }
}

template <typename JsonObject, typename LhsRef>
bool maybeGetInt64(JsonObject const & jsonObj, LhsRef& lhs, const char member[])
{
    if (!jsonObj.HasMember(member) || !jsonObj[member].IsInt64())
        return false;
    else
    {
        lhs = jsonObj[member].GetInt64();
        return true;
    }
}

}



#endif // PIZZARUNNERJSONHELPERS_H
