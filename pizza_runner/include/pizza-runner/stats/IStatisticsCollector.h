#ifndef ISTATISTICSCOLLECTOR_H
#define ISTATISTICSCOLLECTOR_H

#include <rapidjson/document.h>
#include <memory>
#include <vector>

namespace pizza::runner::config
{
class Config;
}

namespace pizza::runner::statistics
{

class IStatisticsCollector
{
    using Ptr = std::unique_ptr<IStatisticsCollector>;
public:
    enum class Type
    {
        Regular,
        Verbose
    };
    IStatisticsCollector(Type tp) noexcept;
    virtual ~IStatisticsCollector() = default;
    virtual void collectStatistics(const pizza::runner::config::Config&) = 0;
    virtual void toJSON(rapidjson::Document& doc) = 0;
    virtual bool fromJSON(const rapidjson::Document& doc) = 0;
    virtual void clear() noexcept {}
    Type type() const noexcept
    {
        return m_type;
    }
private:
    Type m_type;
};

}
#endif // ISTATISTICSCOLLECTOR_H
