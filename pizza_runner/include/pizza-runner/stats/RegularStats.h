#ifndef NORMALSTATS_H
#define NORMALSTATS_H

#include <string>

namespace pizza::runner::statistics
{

struct RegularStats
{
    std::string m_configName;
    int64_t     m_runningBrokersCount;
    int64_t     m_allBrokersCount;
    int64_t     m_runningBalancersCount;
    int64_t     m_allBalancersCount;
    int64_t     m_runningServicesCount;
    int64_t     m_allServicesCount;
    int64_t     m_runningTime;
};

}


#endif // NORMALSTATS_H
