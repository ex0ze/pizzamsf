#ifndef NORMALSTATISTICSCOLLECTOR_H
#define NORMALSTATISTICSCOLLECTOR_H

#include "IStatisticsCollector.h"
#include "RegularStats.h"

namespace pizza::runner {
class Runner;
}

namespace pizza::runner::statistics
{

class RegularStatisticsCollector : public IStatisticsCollector
{
public:
    using Ptr = std::unique_ptr<RegularStatisticsCollector>;


    // IStatisticsCollector interface
public:
    static Ptr create() { return Ptr(new RegularStatisticsCollector()); }
    static Ptr create(const Runner& r) { return Ptr(new RegularStatisticsCollector(r)); }
    virtual void collectStatistics(const config::Config & conf) override;
    virtual void toJSON(rapidjson::Document &doc) override;
    virtual bool fromJSON(const rapidjson::Document &doc) override;
    virtual void clear() noexcept override;
    const std::vector<RegularStats>& collectedStats() const noexcept;
private:
    std::vector<RegularStats> m_stats;
    RegularStatisticsCollector();
    RegularStatisticsCollector(const Runner& r);
};

}
#endif // NORMALSTATISTICSCOLLECTOR_H
