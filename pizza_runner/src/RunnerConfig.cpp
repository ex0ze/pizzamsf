#include <iostream>
#include <fstream>
#include <rapidjson/document.h>
#include <rapidjson/error/en.h>
#include "../include/pizza-runner/RunnerConfig.h"
#include "../include/pizza-runner/Exception.hpp"
#include "../include/pizza-runner/json/PizzaRunnerJsonHelpers.h"
#include "../include/pizza-runner/RunnerConfigFormat.hpp"
#include "../include/pizza-runner/stats/IStatisticsCollector.h"

bool pizza::runner::config::Config::load(std::string_view path)
{
    namespace pr = pizza::runner;
    namespace ex = pr::exception;
    bool result = true;
    try
    {
        result = m_loader.load(path);
    }
    catch (ex::Exception& e)
    {
        std::cout << e;
        return false;
    }
    catch (std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return false;
    }
    if (!result)
        return false;
    m_configPath = path;
    m_configName = m_loader.consumeName();
    m_entries = m_loader.consumeResult();
    return true;
}

bool pizza::runner::config::Config::run()
{
    bool result = true;
    for (auto& e : m_entries)
    {
        result &= e->run();
        if (!result)
            break;
    }
    if (!result)
    {
        terminate();
        return false;
    }
    else
    {
        setLaunchedAt();
            return true;
    }
}

void pizza::runner::config::Config::terminate()
{
    for (auto& e : m_entries)
        e->terminate();
    m_launching = false;
}

std::optional<pizza::runner::config::Config> pizza::runner::config::Config::fromFile(std::string_view path)
{
    Config conf;
    if (!conf.load(path))
        return std::nullopt;
    else
        return conf;
}

std::string pizza::runner::config::Config::configName() const
{
    return m_configName;
}

int64_t pizza::runner::config::Config::launchedAt() const noexcept
{
    return m_launchedAt;
}

int64_t pizza::runner::config::Config::currentRunTime() const noexcept
{
    if (!m_launching)
        return 0;
    else
        return pizza::helpers::currentRelativeClock() - launchedAt();
}

std::string pizza::runner::config::Config::path() const
{
    return m_configPath.string();
}

void pizza::runner::config::Config::setLaunchedAt()
{
    m_launching = true;
    m_launchedAt = pizza::helpers::currentRelativeClock();
}




