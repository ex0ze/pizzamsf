#include "../include/pizza-runner/helpers.h"

std::optional<std::string> pizza::helpers::readFile(std::string_view path)
{
    const std::filesystem::path p {path};
    if (!std::filesystem::exists(p))
    {
        return std::nullopt;
    }
    std::ifstream ifs(p, std::ios::in | std::ios::binary);
    if (!ifs.is_open())
        return std::nullopt;
    ifs.seekg(0, std::ios::end);
    const auto size = ifs.tellg();
    ifs.seekg(0, std::ios::beg);
    std::string res;
    res.resize(size);
    ifs.read(res.data(), size);
    ifs.close();
    return res;
}

int64_t pizza::helpers::currentRelativeClock()
{
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
}

int64_t pizza::helpers::currentAbsoluteClock()
{
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}
