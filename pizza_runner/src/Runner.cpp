#include "../include/pizza-runner/Runner.h"
#include "../include/pizza-runner/stats/IStatisticsCollector.h"

bool pizza::runner::Runner::addConfig(pizza::runner::config::Config conf)
{
    if (auto it = m_configHash.find(conf.configName()); it == m_configHash.end())
    {
        m_configHash.emplace(conf.configName(), std::move(conf));
        return true;
    }
    else
        return false;
}

bool pizza::runner::Runner::removeConfig(const std::string &confName)
{
    return (m_configHash.erase(confName) == 1);
}

bool pizza::runner::Runner::runConfig(const std::string& confName)
{
    if (auto it = m_configHash.find(confName); it != m_configHash.end())
    {
        return (*it).second.run();
    }
    return false;
}

bool pizza::runner::Runner::stopConfig(const std::string& confName)
{
    if (auto it = m_configHash.find(confName); it != m_configHash.end())
    {
        (*it).second.terminate();
        return true;
    }
    return false;
}

void pizza::runner::Runner::acceptStatisticsCollector(pizza::runner::statistics::IStatisticsCollector & c) const
{
    for (auto const& [name, cfg] : m_configHash)
        c.collectStatistics(cfg);
}

const std::unordered_map<std::string, pizza::runner::config::Config> &pizza::runner::Runner::getConfigs() const noexcept
{
    return m_configHash;
}

pizza::runner::config::Config *pizza::runner::Runner::config(const std::string &confName) noexcept
{
    if (auto it = m_configHash.find(confName); it != std::end(m_configHash))
    {
        return &it->second;
    }
    else
        return nullptr;
}

