#include "../include/pizza-runner/ConfigLoader.h"
#include "../include/pizza-runner/Exception.hpp"
#include "../include/pizza-runner/RunnerConfigFormat.hpp"
#include "../include/pizza-runner/helpers.h"
#include "../include/pizza-runner/entry/Broker.h"
#include "../include/pizza-runner/entry/Balancer.h"
#include "../include/pizza-runner/entry/Service.h"
#include "../include/pizza-runner/json/PizzaRunnerJsonHelpers.h"
#include <rapidjson/error/en.h>
#include <iostream>

std::vector<pizza::runner::entry::IEntry::Ptr> &&pizza::runner::config::ConfigLoader::consumeResult() noexcept
{
    return std::move(m_result);
}

std::string &&pizza::runner::config::ConfigLoader::consumeName() noexcept
{
    return std::move(m_configName);
}

bool pizza::runner::config::ConfigLoader::load(std::string_view path)
{
    m_configName.clear();
    m_result.clear();
    namespace pr = pizza::runner;
    namespace ex = pr::exception;
    auto maybeFileContents = pizza::helpers::readFile(path);
    if (!maybeFileContents.has_value())
        return false;
    auto fileContents = std::move(maybeFileContents.value());
    rapidjson::Document doc;
    const rapidjson::ParseResult result = doc.Parse(fileContents.c_str(), fileContents.length());
    if (result.IsError())
    {
        std::cout << "Error parsing json\n";
        std::cout << "What: " << rapidjson::GetParseError_En(result.Code()) << std::endl;
        std::cout << "File: " << path << std::endl;
        return false;
    }

    if (!pr::helpers::json::maybeGetString(doc, m_configName, "config"))
    {
        std::cout << pizza::runner::exception::Exception("[Pizza runner config] Config", ex::common_ex::kMissingField, pr::format::kConfig);
        return false;
    }
    return loadBrokers(doc) && loadServicesAndBalancers(doc);
}

bool pizza::runner::config::ConfigLoader::loadBrokers(rapidjson::Document & doc)
{
    namespace ex = pizza::runner::exception;
    namespace fmt = pizza::runner::format;
    namespace ent = pizza::runner::entry;
    namespace helpers = pizza::runner::helpers::json;
    if (!doc.HasMember(fmt::kBrokers))
        return true;
    for (const auto& curr : doc[fmt::kBrokers].GetArray())
    {
        auto b = std::make_unique<ent::Broker>();
        if (!helpers::maybeGetString(curr, b->m_addr, fmt::brokers::kAddr))
            throw ex::Exception("[Pizza runner config] Broker", ex::common_ex::kMissingField, fmt::brokers::kAddr);
        if (!helpers::maybeGetString(curr, b->m_executable, fmt::brokers::kExecutable))
            throw ex::Exception("[Pizza runner config] Broker", ex::common_ex::kMissingField, fmt::brokers::kExecutable);
        if (!helpers::maybeGetInt(curr, b->m_ioThreads, fmt::brokers::kIoThreads))
        {
            b->m_ioThreads = 1;
        }
        m_result.emplace_back(std::move(b));
    }
    return true;
}

bool pizza::runner::config::ConfigLoader::loadServicesAndBalancers(rapidjson::Document & doc)
{
    namespace ex = pizza::runner::exception;
    namespace fmt = pizza::runner::format;
    namespace ent = pizza::runner::entry;
    namespace helpers = pizza::runner::helpers::json;
    if (!doc.HasMember(fmt::kServices))
        return true;
    for (const auto& curr : doc[fmt::kServices].GetArray())
    {
        auto s = std::make_unique<ent::Service>();
        if (!helpers::maybeGetString(curr, s->m_name, fmt::services::kName))
            throw ex::Exception("[Pizza runner config] Service", ex::common_ex::kMissingField, fmt::services::kName);
        if (!helpers::maybeGetString(curr, s->m_executable, fmt::services::kExecutable))
            throw ex::Exception("[Pizza runner config] Service", ex::common_ex::kMissingField, fmt::services::kExecutable);
        if (!helpers::maybeGetInt(curr, s->m_count, fmt::services::kCount))
        {
            s->m_count = 1;
        }
        if (!helpers::maybeGetString(curr, s->m_brokerAddr, fmt::services::kBrokerAddr))
            throw ex::Exception("[Pizza runner config] Service", ex::common_ex::kMissingField, fmt::services::kBrokerAddr);
        bool hasBalancer = curr.HasMember(fmt::services::kBalancer);
        if (!hasBalancer && s->m_count > 1)
            throw ex::Exception("[Pizza runner config] Balancer", ex::common_ex::kMissingBalancer);
        else if (hasBalancer)
        {
            auto balancer = std::make_unique<ent::Balancer>();
            balancer->m_name = s->m_name;
            balancer->m_frontendAddr = s->m_brokerAddr;
            if (!helpers::maybeGetString(curr[fmt::services::kBalancer], balancer->m_backendAddr, fmt::balancer::kAddr))
                throw ex::Exception("[Pizza runner config] Balancer", ex::common_ex::kMissingField, fmt::balancer::kAddr);
            if (!helpers::maybeGetString(curr[fmt::services::kBalancer], balancer->m_executable, fmt::balancer::kExecutable))
                throw ex::Exception("[Pizza runner config] Balancer", ex::common_ex::kMissingField, fmt::balancer::kExecutable);
            s->setBalancerAddr(balancer->m_backendAddr);
            m_result.emplace_back(std::move(balancer));
        }
        if (curr.HasMember(fmt::services::kClArgs))
        {
            for (auto it = curr[fmt::services::kClArgs].MemberBegin(); it != curr[fmt::services::kClArgs].MemberEnd(); ++it)
            {
                if (!(*it).value.IsString())
                    throw ex::Exception("[Pizza runner config] Service", ex::common_ex::kWrongArgument, "Command line argument must be a string type");
                s->m_clArgs[(*it).name.GetString()] = (*it).value.GetString();
            }
        }
        m_result.emplace_back(std::move(s));
    }
    return true;
}
