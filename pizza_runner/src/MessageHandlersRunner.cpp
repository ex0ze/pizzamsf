#include "../include/pizza-runner/MessageHandlersRunner.h"
#include "../include/pizza-runner/ServerMessageFormat.hpp"
#include "../include/pizza-runner/message/JSONMessageCreator.h"
#include "../include/pizza-runner/message/ErrorReplyMessage.h"

namespace pizza::runner::srv::handlers {

bool HandlersRunner::registerHandler(message::IMessage::MessagePacket packet, message::IMessage::MessageAction act, HandlersRunner::handler_t handler)
{
    if (auto it = m_handlers.find(std::make_pair(packet, act)); it == m_handlers.end())
    {
        m_handlers.emplace_hint(it, std::make_pair(packet, act), std::move(handler));
        return true;
    }
    else
        return false;
}

std::string HandlersRunner::processMessage(std::string_view json)
{
    auto message = message::JSONMessageCreator::instance().fromJSON(json);
    auto errorMessage = message::ErrorReplyMessage::create();
    auto rawErrorMessage = dynamic_cast<message::ErrorReplyMessage*>(errorMessage.get());
    if (!message)
    {
        return rawErrorMessage->setWhat("Malformed message").toJSON();
    }
    const auto packet = message->packet();
    const auto action = message->action();
    auto it = m_handlers.find(std::make_pair(packet, action));
    if (it == m_handlers.end())
    {
        return rawErrorMessage->setWhat("Unsupported action").toJSON();
    }
    auto reply = it->second(std::move(message));
    if (reply)
    {
        return reply->toJSON();
    }
    else
    {
        return rawErrorMessage->setWhat("Error handling").toJSON();
    }
}

}
