#include "../../include/pizza-runner/stats/RegularStatisticsCollector.h"
#include "../../include/pizza-runner/RunnerConfig.h"
#include "../../include/pizza-runner/Runner.h"
#include "../../include/pizza-runner/entry/IEntry.h"
#include "../../include/pizza-runner/ServerMessageFormat.hpp"
#include "../../include/pizza-runner/json/PizzaRunnerJsonHelpers.h"
#include <map>

namespace pizza::runner::statistics
{

RegularStatisticsCollector::RegularStatisticsCollector() : IStatisticsCollector(Type::Regular) {}

RegularStatisticsCollector::RegularStatisticsCollector(const Runner &r) : IStatisticsCollector(Type::Regular)
{
    r.acceptStatisticsCollector(*this);
}

void RegularStatisticsCollector::collectStatistics(const config::Config & conf)
{
    RegularStats stats;
    stats.m_configName = conf.m_configName;
    stats.m_runningTime = conf.currentRunTime();

    std::map<entry::IEntry::Type, std::pair<int64_t, int64_t>> counter; // running - total
    // iterate over entries and count running - total count by type
    for (auto const & e : conf.m_entries)
    {
        ++counter[e->type()].second;
        if (e->running())
            ++counter[e->type()].first;
    }
    using tp = entry::IEntry::Type;
    stats.m_runningBrokersCount = counter[tp::Broker].first;
    stats.m_allBrokersCount = counter[tp::Broker].second;
    stats.m_runningBalancersCount = counter[tp::Balancer].first;
    stats.m_allBalancersCount = counter[tp::Balancer].second;
    stats.m_runningServicesCount = counter[tp::Service].first;
    stats.m_allServicesCount = counter[tp::Service].second;
    m_stats.push_back(std::move(stats));
}

void RegularStatisticsCollector::toJSON(rapidjson::Document &doc)
{
    namespace srv_fmt = pizza::runner::srv::format;
    namespace st = srv_fmt::reply::statistics;
    namespace rg = st::regular;
    if (!doc.IsObject())
        doc.SetObject();
    auto arr = rapidjson::Value();
    arr.SetArray();
    for (const auto& e : m_stats)
    {
        auto obj = rapidjson::Value();
        obj.SetObject();
        obj.AddMember(rapidjson::Value().SetString(rg::kName), rapidjson::Value().SetString(e.m_configName.c_str(), e.m_configName.length()), doc.GetAllocator());
        obj.AddMember(rapidjson::Value().SetString(rg::kRunningTime), rapidjson::Value().SetInt64(e.m_runningTime), doc.GetAllocator());

        obj.AddMember(rapidjson::Value().SetString(rg::kRunningBrokers), rapidjson::Value().SetInt64(e.m_runningBrokersCount), doc.GetAllocator());
        obj.AddMember(rapidjson::Value().SetString(rg::kNumBrokers), rapidjson::Value().SetInt64(e.m_allBrokersCount), doc.GetAllocator());

        obj.AddMember(rapidjson::Value().SetString(rg::kRunningBalancers), rapidjson::Value().SetInt64(e.m_runningBalancersCount), doc.GetAllocator());
        obj.AddMember(rapidjson::Value().SetString(rg::kNumBalancers), rapidjson::Value().SetInt64(e.m_allBalancersCount), doc.GetAllocator());

        obj.AddMember(rapidjson::Value().SetString(rg::kRunningServices), rapidjson::Value().SetInt64(e.m_runningServicesCount), doc.GetAllocator());
        obj.AddMember(rapidjson::Value().SetString(rg::kNumServices), rapidjson::Value().SetInt64(e.m_allServicesCount), doc.GetAllocator());
        arr.GetArray().PushBack(obj.Move(), doc.GetAllocator());
    }
    if (doc.HasMember(srv_fmt::action::kCollectRegularStatistics))
        doc[srv_fmt::action::kCollectRegularStatistics] = std::move(arr);
    else
        doc.AddMember(srv_fmt::action::kCollectRegularStatistics, arr.Move(), doc.GetAllocator());
}

bool RegularStatisticsCollector::fromJSON(const rapidjson::Document &doc)
{
    namespace srv_fmt = pizza::runner::srv::format;
    namespace hlp = pizza::runner::helpers::json;
    namespace st = srv_fmt::reply::statistics;
    namespace rg = st::regular;
    if (!doc.HasMember(srv_fmt::action::kCollectRegularStatistics))
        return false;
    for (const auto& obj : doc[srv_fmt::action::kCollectRegularStatistics].GetArray())
    {
        if (!hlp::hasAllMembers(obj,
                rg::kName,
                rg::kNumBalancers,
                rg::kNumBrokers,
                rg::kNumServices,
                rg::kRunningBalancers,
                rg::kRunningBrokers,
                rg::kRunningServices,
                rg::kRunningTime))
        {
            m_stats.clear();
            return false;
        }
        if (!hlp::isAllMembersInt64(obj,
                rg::kNumBalancers,
                rg::kNumBrokers,
                rg::kNumServices,
                rg::kRunningBalancers,
                rg::kRunningBrokers,
                rg::kRunningServices,
                rg::kRunningTime))
        {
            m_stats.clear();
            return false;
        }

        RegularStats stats;
        stats.m_configName = obj[rg::kName].GetString();
        stats.m_allBalancersCount = obj[rg::kNumBalancers].GetInt64();
        stats.m_allBrokersCount = obj[rg::kNumBrokers].GetInt64();
        stats.m_allServicesCount = obj[rg::kNumServices].GetInt64();
        stats.m_runningBalancersCount = obj[rg::kRunningBalancers].GetInt64();
        stats.m_runningBrokersCount = obj[rg::kRunningBrokers].GetInt64();
        stats.m_runningServicesCount = obj[rg::kRunningServices].GetInt64();
        stats.m_runningTime = obj[rg::kRunningTime].GetInt64();
        m_stats.push_back(std::move(stats));
    }
    return true;
}

void RegularStatisticsCollector::clear() noexcept
{
    m_stats.clear();
}

const std::vector<RegularStats> &RegularStatisticsCollector::collectedStats() const noexcept
{
    return m_stats;
}
}
