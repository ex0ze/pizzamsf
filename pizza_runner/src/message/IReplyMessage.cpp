#include "../../include/pizza-runner/message/IReplyMessage.h"

namespace pizza::runner::message
{

const std::map<IReplyMessage::MessageStatus, std::string_view> IReplyMessage::statusViewTable =
    {
        {IReplyMessage::MessageStatus::Success, srv::format::reply::status::kSuccess},
        {IReplyMessage::MessageStatus::Failed, srv::format::reply::status::kFailed}
    };
}

pizza::runner::message::IReplyMessage::IReplyMessage(pizza::runner::message::IMessage::MessageAction act) :
IMessage(IMessage::MessagePacket::Reply, act) {}

const std::string &pizza::runner::message::IReplyMessage::what() const noexcept
{
    return m_what;
}

pizza::runner::message::IReplyMessage &pizza::runner::message::IReplyMessage::setWhat(std::string what)
{
    m_what = std::move(what);
    return *this;
}


pizza::runner::message::IReplyMessage::MessageStatus pizza::runner::message::IReplyMessage::status() const noexcept
{
    return m_status;
}

pizza::runner::message::IReplyMessage &pizza::runner::message::IReplyMessage::setStatus(pizza::runner::message::IReplyMessage::MessageStatus status) noexcept
{
    m_status = status;
    return *this;
}



std::optional<std::string_view> pizza::runner::message::IReplyMessage::recognizeStatus(pizza::runner::message::IReplyMessage::MessageStatus status) noexcept
{
    if (auto it = statusViewTable.find(status); it != std::end(statusViewTable))
        return (*it).second;
    else
        return std::nullopt;
}

std::optional<pizza::runner::message::IReplyMessage::MessageStatus> pizza::runner::message::IReplyMessage::recognizeStatus(std::string_view status) noexcept
{
    for (const auto& it : statusViewTable)
    {
        if (it.second == status)
            return it.first;
    }
    return std::nullopt;
}

bool pizza::runner::message::IReplyMessage::fromJSON(const rapidjson::Document & doc)
{
    return readFields(doc);
}

std::string pizza::runner::message::IReplyMessage::toJSON()
{
    writeFields();
    return IMessage::toJSON();
}

void pizza::runner::message::IReplyMessage::writeFields()
{
    Base::writeFields();
    namespace srv_fmt = pizza::runner::srv::format;

    auto jkStatus = rapidjson::Value();
    jkStatus.SetString(srv_fmt::reply::kStatus);

    auto jvStatus = rapidjson::Value();
    auto statusStr = recognizeStatus(m_status).value();
    jvStatus.SetString(statusStr.data(), statusStr.length());

    auto jkWhat = rapidjson::Value();
    jkWhat.SetString(srv_fmt::reply::kReason);

    auto jvWhat = rapidjson::Value();
    if (!m_what.empty())
        jvWhat.SetString(m_what.c_str(), m_what.length());
    else
        jvWhat.SetString("");

    p_doc->AddMember(jkStatus, jvStatus, p_doc->GetAllocator());
    p_doc->AddMember(jkWhat, jvWhat, p_doc->GetAllocator());
}

bool pizza::runner::message::IReplyMessage::readFields(const rapidjson::Document & doc)
{
    if (!IMessage::readFields(doc))
        return false;
    namespace jh = pizza::runner::helpers::json;
    namespace fmt = pizza::runner::srv::format;
    std::string_view s_status;
    std::string s_what;
    if (!jh::maybeGetStringView(doc, s_status, fmt::reply::kStatus))
        return false;
    //! reason is not necessary
    jh::maybeGetString(doc, s_what, fmt::reply::kReason);
    auto maybeStatus = recognizeStatus(s_status);
    if (!maybeStatus.has_value())
        return false;

    m_status = maybeStatus.value();
    if (!s_what.empty())
        m_what = std::move(s_what);
    return true;
}
