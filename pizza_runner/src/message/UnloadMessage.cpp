#include "../../include/pizza-runner/message/UnloadMessage.h"

pizza::runner::message::RequestUnloadMessage::RequestUnloadMessage() : RequestMessageWithName(IMessage::MessageAction::Unload) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestUnloadMessage::create() { return std::make_unique<RequestUnloadMessage>(); }

pizza::runner::message::ReplyUnloadMessage::ReplyUnloadMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Unload)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyUnloadMessage::create() { return std::make_unique<ReplyUnloadMessage>(); }
