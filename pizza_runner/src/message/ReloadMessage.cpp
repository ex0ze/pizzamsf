#include "../../include/pizza-runner/message/ReloadMessage.h"

pizza::runner::message::RequestReloadMessage::RequestReloadMessage() : RequestMessageWithName(IMessage::MessageAction::Reload) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestReloadMessage::create() { return std::make_unique<RequestReloadMessage>(); }

pizza::runner::message::ReplyReloadMessage::ReplyReloadMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Reload)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyReloadMessage::create() { return std::make_unique<ReplyReloadMessage>(); }
