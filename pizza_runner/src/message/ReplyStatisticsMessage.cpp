#include "../../include/pizza-runner/message/ReplyStatisticsMessage.h"

pizza::runner::message::ReplyStatisticsMessage &pizza::runner::message::ReplyStatisticsMessage::setCollector(std::unique_ptr<pizza::runner::statistics::IStatisticsCollector> c) noexcept
{
    p_statsCollector = std::move(c);
    return *this;
}

pizza::runner::statistics::IStatisticsCollector *pizza::runner::message::ReplyStatisticsMessage::collector() noexcept { return p_statsCollector.get(); }

std::unique_ptr<pizza::runner::statistics::IStatisticsCollector> pizza::runner::message::ReplyStatisticsMessage::consumeCollector() noexcept
{
    return std::move(p_statsCollector);
}

std::string pizza::runner::message::ReplyStatisticsMessage::toJSON()
{
    writeFields();
    return IReplyMessage::toJSON();
}

void pizza::runner::message::ReplyStatisticsMessage::writeFields()
{
    Base::writeFields();
    if (!p_statsCollector)
        return;
    p_statsCollector->toJSON(*p_doc);
}
