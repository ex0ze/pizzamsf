#include "../../include/pizza-runner/message/RestartMessage.h"

pizza::runner::message::RequestRestartMessage::RequestRestartMessage() : RequestMessageWithName(IMessage::MessageAction::Restart) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestRestartMessage::create() { return std::make_unique<RequestRestartMessage>(); }

pizza::runner::message::ReplyRestartMessage::ReplyRestartMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Restart)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyRestartMessage::create() { return std::make_unique<ReplyRestartMessage>(); }
