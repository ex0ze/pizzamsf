#include "../../include/pizza-runner/message/RunMessage.h"

pizza::runner::message::RequestRunMessage::RequestRunMessage() : RequestMessageWithName(IMessage::MessageAction::Run) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestRunMessage::create() { return std::make_unique<RequestRunMessage>(); }

pizza::runner::message::ReplyRunMessage::ReplyRunMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Run)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyRunMessage::create() { return std::make_unique<ReplyRunMessage>(); }
