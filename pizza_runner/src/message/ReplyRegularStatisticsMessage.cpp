#include "../../include/pizza-runner/message/ReplyRegularStatisticsMessage.h"
#include "../../include/pizza-runner/stats/RegularStatisticsCollector.h"

pizza::runner::message::ReplyRegularStatisticsMessage::ReplyRegularStatisticsMessage() : ReplyStatisticsMessage(MessageAction::CollectRegularStatistics) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyRegularStatisticsMessage::create() { return std::make_unique<ReplyRegularStatisticsMessage>(); }

bool pizza::runner::message::ReplyRegularStatisticsMessage::fromJSON(const rapidjson::Document & doc)
{
    return readFields(doc);
}

bool pizza::runner::message::ReplyRegularStatisticsMessage::readFields(const rapidjson::Document & doc)
{
    if (!Base::readFields(doc))
        return false;
    p_statsCollector = statistics::RegularStatisticsCollector::create();
    return p_statsCollector->fromJSON(doc);
}
