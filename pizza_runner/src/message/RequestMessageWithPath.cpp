#include "../../include/pizza-runner/message/RequestMessageWithPath.h"

void pizza::runner::message::RequestMessageWithPath::setPath(std::string path)
{
    m_path = std::move(path);
}

const std::string &pizza::runner::message::RequestMessageWithPath::path() const noexcept
{
    return m_path;
}

std::string pizza::runner::message::RequestMessageWithPath::toJSON()
{
    writeFields();
    return IRequestMessage::toJSON();
}

bool pizza::runner::message::RequestMessageWithPath::fromJSON(const rapidjson::Document &doc)
{
    return readFields(doc);
}

void pizza::runner::message::RequestMessageWithPath::writeFields()
{
    Base::writeFields();
    namespace srv_fmt = pizza::runner::srv::format;

    auto jkConfigPath = rapidjson::Value();
    jkConfigPath.SetString(srv_fmt::request::kConfigPath);

    auto jvConfigPath = rapidjson::Value();
    jvConfigPath.SetString(m_path.c_str(), m_path.length());

    p_doc->AddMember(jkConfigPath, jvConfigPath, p_doc->GetAllocator());

}

bool pizza::runner::message::RequestMessageWithPath::readFields(const rapidjson::Document &doc)
{
    if (!Base::readFields(doc))
        return false;
    namespace srv_fmt = pizza::runner::srv::format;
    namespace jh = pizza::runner::helpers::json;
    std::string s_path;
    if (!jh::maybeGetString(doc, s_path, srv_fmt::request::kConfigPath))
        return false;
    m_path = std::move(s_path);
    return true;
}


