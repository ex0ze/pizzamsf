#include "../../include/pizza-runner/message/LoadMessage.h"

pizza::runner::message::RequestLoadMessage::RequestLoadMessage() : RequestMessageWithPath(IMessage::MessageAction::Load) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestLoadMessage::create(){ return std::make_unique<RequestLoadMessage>(); }

pizza::runner::message::ReplyLoadMessage::ReplyLoadMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Load)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyLoadMessage::create() { return std::make_unique<ReplyLoadMessage>(); }
