#include "../../include/pizza-runner/message/StopMessage.h"

pizza::runner::message::RequestStopMessage::RequestStopMessage() : RequestMessageWithName(IMessage::MessageAction::Stop) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestStopMessage::create() { return std::make_unique<RequestStopMessage>(); }

pizza::runner::message::ReplyStopMessage::ReplyStopMessage(pizza::runner::message::IReplyMessage::MessageStatus status, std::string what)
: IReplyMessage(IMessage::MessageAction::Stop)
{
    setStatus(status).setWhat(std::move(what));
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ReplyStopMessage::create() { return std::make_unique<ReplyStopMessage>(); }
