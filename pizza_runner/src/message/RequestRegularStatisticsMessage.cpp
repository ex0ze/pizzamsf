#include "../../include/pizza-runner/message/RequestRegularStatisticsMessage.h"

pizza::runner::message::RequestRegularStatisticsMessage::RequestRegularStatisticsMessage() :
IRequestMessage(MessageAction::CollectRegularStatistics) {}

pizza::runner::message::IMessage::Ptr pizza::runner::message::RequestRegularStatisticsMessage::create() { return std::make_unique<RequestRegularStatisticsMessage>(); }
