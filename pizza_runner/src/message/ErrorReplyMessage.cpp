#include "../../include/pizza-runner/message/ErrorReplyMessage.h"

pizza::runner::message::ErrorReplyMessage::ErrorReplyMessage() : IReplyMessage(IMessage::MessageAction::Unknown)
{
    setStatus(IReplyMessage::MessageStatus::Failed);
}

pizza::runner::message::IMessage::Ptr pizza::runner::message::ErrorReplyMessage::create() { return std::make_unique<ErrorReplyMessage>(); }
