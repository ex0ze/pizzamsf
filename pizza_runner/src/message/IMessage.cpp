#include "../../include/pizza-runner/message/IMessage.h"

pizza::runner::message::IMessage::IMessage(pizza::runner::message::IMessage::MessagePacket packet, pizza::runner::message::IMessage::MessageAction action)
: m_packet(packet), m_action(action), p_doc(std::make_unique<rapidjson::Document>()) {}

std::string pizza::runner::message::IMessage::toJSON()
{
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    p_doc->Accept(writer);
    return buf.GetString();
}

bool pizza::runner::message::IMessage::fromJSON(const rapidjson::Document & doc)
{
    return readFields(doc);
}


pizza::runner::message::IMessage::MessagePacket pizza::runner::message::IMessage::packet() const noexcept
{
    return m_packet;
}

pizza::runner::message::IMessage::MessageAction pizza::runner::message::IMessage::action() const noexcept
{
    return m_action;
}

std::optional<pizza::runner::message::IMessage::MessageAction> pizza::runner::message::IMessage::recognizeAction(std::string_view action) noexcept
{
    for (const auto& it : actionViewTable)
    {
        if (it.second == action)
            return it.first;
    }
    return std::nullopt;
}

std::optional<std::string_view> pizza::runner::message::IMessage::recognizeAction(pizza::runner::message::IMessage::MessageAction action) noexcept
{
    if (auto it = actionViewTable.find(action); it != std::cend(actionViewTable))
        return (*it).second;
    else
        return std::nullopt;
}

std::optional<pizza::runner::message::IMessage::MessagePacket> pizza::runner::message::IMessage::recognizePacket(std::string_view packet) noexcept
{
    for (const auto& it : packetViewTable)
    {
        if (it.second == packet)
            return it.first;
    }
    return std::nullopt;
}

std::optional<std::string_view> pizza::runner::message::IMessage::recognizePacket(pizza::runner::message::IMessage::MessagePacket packet) noexcept
{
    if (auto it = packetViewTable.find(packet); it != std::cend(packetViewTable))
        return (*it).second;
    else
        return std::nullopt;
}

rapidjson::Document *pizza::runner::message::IMessage::getDocument() noexcept
{
    return p_doc.get();
}

const std::map<pizza::runner::message::IMessage::MessageAction, std::string_view> pizza::runner::message::IMessage::actionViewTable =
    {
        {MessageAction::Unknown,                    pizza::runner::srv::format::action::kUnknownAction},
        {MessageAction::Load,                       pizza::runner::srv::format::action::kLoad   },
        {MessageAction::Unload,                     pizza::runner::srv::format::action::kUnload },
        {MessageAction::Run,                        pizza::runner::srv::format::action::kRun    },
        {MessageAction::Stop,                       pizza::runner::srv::format::action::kStop   },
        {MessageAction::Restart,                    pizza::runner::srv::format::action::kRestart},
        {MessageAction::Reload,                     pizza::runner::srv::format::action::kReload },
        {MessageAction::CollectRegularStatistics,   pizza::runner::srv::format::action::kCollectRegularStatistics},
        {MessageAction::CollectVerboseStatistics,   pizza::runner::srv::format::action::kCollectVerboseStatistics}
    };

const std::map<pizza::runner::message::IMessage::MessagePacket, std::string_view> pizza::runner::message::IMessage::packetViewTable =
    {
        {MessagePacket::Request,        pizza::runner::srv::format::packet::kRequest},
        {MessagePacket::Reply,        pizza::runner::srv::format::packet::kReply    }
    };

void pizza::runner::message::IMessage::writeFields()
{
    namespace srv_fmt = pizza::runner::srv::format;

    p_doc->SetObject();

    auto jkPacket = rapidjson::Value();
    jkPacket.SetString(srv_fmt::kPacket);

    auto jvPacket = rapidjson::Value();
    auto packetStr = recognizePacket(m_packet).value();
    jvPacket.SetString(packetStr.data(), packetStr.length());

    auto jkAction = rapidjson::Value();
    jkAction.SetString(srv_fmt::kAction);

    auto jvAction = rapidjson::Value();
    auto actionStr = recognizeAction(m_action).value();
    jvAction.SetString(actionStr.data(), actionStr.length());

    p_doc->AddMember(jkPacket, jvPacket, p_doc->GetAllocator());
    p_doc->AddMember(jkAction, jvAction, p_doc->GetAllocator());
}

bool pizza::runner::message::IMessage::readFields(const rapidjson::Document & doc)
{
    namespace jh = pizza::runner::helpers::json;
    namespace fmt = pizza::runner::srv::format;
    std::string_view s_packet, s_action;
    if (!jh::maybeGetStringView(doc, s_packet, fmt::kPacket))
        return false;
    if (!jh::maybeGetStringView(doc, s_action, fmt::kAction))
        return false;
    auto maybePacket = recognizePacket(s_packet);
    if (!maybePacket.has_value())
        return false;
    m_packet = maybePacket.value();
    auto maybeAction = recognizeAction(s_action);
    if (!maybeAction.has_value())
        return false;
    m_action = maybeAction.value();
    return true;
}
