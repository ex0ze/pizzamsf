#include "../../include/pizza-runner/message/RequestMessageWithName.h"

void pizza::runner::message::RequestMessageWithName::setName(std::string name)
{
    m_name = std::move(name);
}

const std::string &pizza::runner::message::RequestMessageWithName::name() const noexcept
{
    return m_name;
}

std::string pizza::runner::message::RequestMessageWithName::toJSON()
{
    writeFields();
    return IRequestMessage::toJSON();
}

bool pizza::runner::message::RequestMessageWithName::fromJSON(const rapidjson::Document &doc)
{
    return readFields(doc);
}

void pizza::runner::message::RequestMessageWithName::writeFields()
{
    Base::writeFields();
    namespace srv_fmt = pizza::runner::srv::format;

    auto jkConfigName = rapidjson::Value();
    jkConfigName.SetString(srv_fmt::request::kConfigName);

    auto jvConfigName = rapidjson::Value();
    jvConfigName.SetString(m_name.c_str(), m_name.length());

    p_doc->AddMember(jkConfigName, jvConfigName, p_doc->GetAllocator());
}

bool pizza::runner::message::RequestMessageWithName::readFields(const rapidjson::Document &doc)
{
    if (!Base::readFields(doc))
        return false;
    namespace srv_fmt = pizza::runner::srv::format;
    namespace jh = pizza::runner::helpers::json;
    std::string s_name;
    if (!jh::maybeGetString(doc, s_name, srv_fmt::request::kConfigName))
        return false;
    m_name = std::move(s_name);
    return true;
}
