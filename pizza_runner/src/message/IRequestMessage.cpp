#include "../../include/pizza-runner/message/IRequestMessage.h"

pizza::runner::message::IRequestMessage::IRequestMessage(pizza::runner::message::IMessage::MessageAction act) : IMessage(IMessage::MessagePacket::Request, act) {}

std::string pizza::runner::message::IRequestMessage::toJSON()
{
    writeFields();
    return IMessage::toJSON();
}

bool pizza::runner::message::IRequestMessage::fromJSON(const rapidjson::Document &doc)
{
    return readFields(doc);
}

void pizza::runner::message::IRequestMessage::writeFields()
{
    Base::writeFields();
    // no fields to write
}

bool pizza::runner::message::IRequestMessage::readFields(const rapidjson::Document &doc)
{
    return Base::readFields(doc);
    // no fields to read
}
