#include "../../include/pizza-runner/message/JSONMessageCreator.h"
#include "../../include/pizza-runner/message/LoadMessage.h"
#include "../../include/pizza-runner/message/ReloadMessage.h"
#include "../../include/pizza-runner/message/RestartMessage.h"
#include "../../include/pizza-runner/message/RunMessage.h"
#include "../../include/pizza-runner/message/ErrorReplyMessage.h"
#include "../../include/pizza-runner/message/StopMessage.h"
#include "../../include/pizza-runner/message/UnloadMessage.h"
#include "../../include/pizza-runner/message/ReplyRegularStatisticsMessage.h"
#include "../../include/pizza-runner/message/RequestRegularStatisticsMessage.h"

namespace pizza::runner::message {

IMessage::Ptr JSONMessageCreator::fromJSON(std::string_view json) const
{
    namespace jh = pizza::runner::helpers::json;
    namespace fmt = pizza::runner::srv::format;

    rapidjson::Document doc;
    const rapidjson::ParseResult result = doc.Parse(json.data(), json.length());
    if (result.IsError())
        return {};
    std::string_view s_packet, s_action;
    if (!jh::maybeGetStringView(doc, s_packet, fmt::kPacket))
        return {};
    if (!jh::maybeGetStringView(doc, s_action, fmt::kAction))
        return {};
    auto maybeAction = IMessage::recognizeAction(s_action);
    if (!maybeAction.has_value())
        return {};
    auto maybePacket = IMessage::recognizePacket(s_packet);
    if (!maybePacket.has_value())
        return {};
    const auto action = maybeAction.value();
    const auto packet = maybePacket.value();
    auto maybeFn = findFunction(packet, action);
    if (!maybeFn.has_value())
        return {};
    IMessage::Ptr message = maybeFn.value()();
    if (!message->fromJSON(doc))
        return {};
    return message;
}

JSONMessageCreator::JSONMessageCreator()
{
    initializeLookupTable();
}

void JSONMessageCreator::initializeLookupTable()
{
    using act = IMessage::MessageAction;
    using pkt = IMessage::MessagePacket;
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Unknown),                      &ErrorReplyMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Load),                         &RequestLoadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Load),                         &ReplyLoadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Reload),                       &RequestReloadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Reload),                       &ReplyReloadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Restart),                      &RequestRestartMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Restart),                      &ReplyRestartMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Run),                          &RequestRunMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Run),                          &ReplyRunMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Stop),                         &RequestStopMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Stop),                         &ReplyStopMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::Unload),                       &RequestUnloadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::Unload),                       &ReplyUnloadMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Request,   act::CollectRegularStatistics),     &RequestRegularStatisticsMessage::create);
    m_createMessageTable.emplace(std::make_pair(pkt::Reply,     act::CollectRegularStatistics),     &ReplyRegularStatisticsMessage::create);
}

std::optional<IMessage::CreateFn> JSONMessageCreator::findFunction(IMessage::MessagePacket pkt, IMessage::MessageAction act) const noexcept
{
    if (auto it = m_createMessageTable.find(std::make_pair(pkt, act)); it != std::end(m_createMessageTable))
    {
        return (*it).second;
    }
    return std::nullopt;
}

}
