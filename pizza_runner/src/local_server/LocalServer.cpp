#include "../../include/pizza-runner/local_server/LocalServer.h"

pizza::runner::srv::LocalServer::LocalServer(const std::string &bindAddr)
    : m_socket(m_context, zmq::socket_type::rep)
{
    m_socket.bind(bindAddr);
}

std::string pizza::runner::srv::LocalServer::recv()
{
    zmq::message_t res;
    auto ret = m_socket.recv(res);
    return (ret.has_value() ? res.to_string() : std::string());
}

void pizza::runner::srv::LocalServer::reply(const std::string &msg)
{
    m_socket.send(zmq::message_t(msg.data(), msg.size()), zmq::send_flags::none);
}
