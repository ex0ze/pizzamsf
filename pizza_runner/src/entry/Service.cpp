#include "../../include/pizza-runner/entry/Service.h"

std::string pizza::runner::entry::Service::name() const
{
    return m_name;
}

void pizza::runner::entry::Service::setName(std::string name)
{
    m_name = std::move(name);
}

std::string pizza::runner::entry::Service::executable() const
{
    return m_executable;
}

void pizza::runner::entry::Service::setExecutable(std::string executable)
{
    m_executable = std::move(executable);
}

std::string pizza::runner::entry::Service::brokerAddr() const
{
    return m_brokerAddr;
}

void pizza::runner::entry::Service::setBrokerAddr(std::string brokerAddr)
{
    m_brokerAddr = std::move(brokerAddr);
}

int pizza::runner::entry::Service::count() const
{
    return m_count;
}

void pizza::runner::entry::Service::setCount(int count)
{
    m_count = count;
}

std::string pizza::runner::entry::Service::balancerAddr() const
{
    return m_balancerAddr;
}

void pizza::runner::entry::Service::setBalancerAddr(std::string balancerAddr)
{
    m_balancerAddr = std::move(balancerAddr);
}

const std::map<std::string, std::string> &pizza::runner::entry::Service::clArgs() const
{
    return m_clArgs;
}

void pizza::runner::entry::Service::setClArgs(std::map<std::string, std::string> clArgs)
{
    m_clArgs = std::move(clArgs);
}

std::string pizza::runner::entry::Service::executablePath() const
{
    return m_executable;
}

std::string pizza::runner::entry::Service::commandLineArgs() const
{
    std::stringstream ss;
    ss << "--broker-addr " <<  m_brokerAddr << " --balancer-addr " << m_balancerAddr << " --service-name " << m_name << " ";
    for (const auto& [key, value] : m_clArgs)
    {
        ss << key << " " << value << " ";
    }
    return ss.str();
}
