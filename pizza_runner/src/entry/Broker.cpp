#include "../../include/pizza-runner/entry/Broker.h"

std::string pizza::runner::entry::Broker::executable() const
{
    return m_executable;
}

void pizza::runner::entry::Broker::setExecutable(std::string executable)
{
    m_executable = std::move(executable);
}

std::string pizza::runner::entry::Broker::addr() const
{
    return m_addr;
}

void pizza::runner::entry::Broker::setAddr(std::string addr)
{
    m_addr = std::move(addr);
}

int pizza::runner::entry::Broker::ioThreads() const
{
    return m_ioThreads;
}

void pizza::runner::entry::Broker::setIoThreads(int ioThreads)
{
    m_ioThreads = ioThreads;
}

std::string pizza::runner::entry::Broker::commandLineArgs() const
{
    std::stringstream ss;
    ss << "--addr " << m_addr << " --io-threads " << m_ioThreads;
    return ss.str();
}

std::string pizza::runner::entry::Broker::executablePath() const
{
    return m_executable;
}
