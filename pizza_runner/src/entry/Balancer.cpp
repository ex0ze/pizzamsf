#include "../../include/pizza-runner/entry/Balancer.h"

std::string pizza::runner::entry::Balancer::executablePath() const
{
    return m_executable;
}

std::string pizza::runner::entry::Balancer::commandLineArgs() const
{
    std::stringstream ss;
    ss << "--frontend-addr " << m_frontendAddr << " --backend-addr " << m_backendAddr << " --service-name " << m_name;
    return ss.str();
}

std::string pizza::runner::entry::Balancer::name() const
{
    return m_name;
}

void pizza::runner::entry::Balancer::setName(std::string name)
{
    m_name = std::move(name);
}

std::string pizza::runner::entry::Balancer::executable() const
{
    return m_executable;
}

void pizza::runner::entry::Balancer::setExecutable(std::string executable)
{
    m_executable = std::move(executable);
}

std::string pizza::runner::entry::Balancer::frontendAddr() const
{
    return m_frontendAddr;
}

void pizza::runner::entry::Balancer::setFrontendAddr(std::string frontendAddr)
{
    m_frontendAddr = std::move(frontendAddr);
}

std::string pizza::runner::entry::Balancer::backendAddr() const
{
    return m_backendAddr;
}

void pizza::runner::entry::Balancer::setBackendAddr(std::string backendAddr)
{
    m_backendAddr = std::move(backendAddr);
}
