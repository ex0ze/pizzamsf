#include "../../include/pizza-runner/entry/IEntry.h"
#include <variant>
#include <iostream>
#include <boost/process/io.hpp>
#include "../../include/pizza-runner/helpers.h"
#include <filesystem>

bool pizza::runner::entry::IEntry::run(std::chrono::milliseconds waitDuration)
{
    if (executablePath().empty())
        return false;
    if (p_process && p_process->running())
        return true;
    std::filesystem::path log_path;
    bool path_rc = true;
    try
    {
        log_path = std::filesystem::path(executablePath()).replace_extension(".log");
    }
    catch (std::exception& e)
    {
        std::cerr << "Error forming log path for process:\n" << e.what() << std::endl;
        path_rc = false;
    }
    namespace bp = boost::process;
    if (path_rc == true)
    {
        if (!std::filesystem::exists(executablePath()))
        {
            std::cout << "Error: path " << executablePath() << " does not exist" << std::endl;
            return false;
        }
        p_process.reset(new bp::child(executablePath() + " " + commandLineArgs(),
            (bp::std_out & bp::std_err) > log_path));
    }
    else
    {
        //! fallback impl
        p_process.reset(new bp::child(executablePath() + " " + commandLineArgs(),
            (bp::std_out & bp::std_err) > bp::null));
    }
    bool terminated = p_process->wait_for(waitDuration);
    if (!terminated)
    {
        setLaunchedAt();
        return true;
    }
    else
    {
        p_process.reset();
        return false;
    }
}

bool pizza::runner::entry::IEntry::terminate()
{
    m_launchedAt = -1;
    if (!p_process)
        return true;
    if (!p_process->running())
        return true;
    p_process->terminate();
    p_process.reset();
    return true;
}

int64_t pizza::runner::entry::IEntry::launchedAt() const noexcept
{
    return m_launchedAt;
}

int64_t pizza::runner::entry::IEntry::currentRunTime() const noexcept
{
    if (!running())
        return -1;
    return pizza::helpers::currentRelativeClock() - m_launchedAt;
}

bool pizza::runner::entry::IEntry::running() const
{
    if (p_process)
    {
        return p_process->running();
    }
    else
        return false;
}

void pizza::runner::entry::IEntry::setLaunchedAt()
{
    m_launchedAt = pizza::helpers::currentRelativeClock();
}
