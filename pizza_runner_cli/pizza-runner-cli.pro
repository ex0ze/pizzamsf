TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../build/
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
SOURCES += \
        LocalClient.cpp \
        main.cpp


LIBS += -lzmq

HEADERS += \
    LocalClient.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build/release/ -lpizza-runner
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build/debug/ -lpizza-runner
else:unix: LIBS += -L$$PWD/../build/ -lpizza-runner

INCLUDEPATH += $$PWD/../pizza-runner/include
DEPENDPATH += $$PWD/../pizza-runner/include
