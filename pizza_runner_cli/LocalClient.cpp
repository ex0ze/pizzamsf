#include "LocalClient.h"



LocalClient::LocalClient(const std::string &connectAddr) : m_socket(m_context, zmq::socket_type::req)
{
    m_socket.connect(connectAddr);
}

void LocalClient::send(const std::string &str)
{
    m_socket.send(zmq::message_t(str.data(), str.size()), zmq::send_flags::none);
}

std::string LocalClient::recv()
{
    zmq::message_t msg;
    auto res = m_socket.recv(msg);
    return (res.has_value() ? msg.to_string() : std::string());
}
