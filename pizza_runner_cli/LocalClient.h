#ifndef LOCALCLIENT_H
#define LOCALCLIENT_H

#include "../thirdparty/cppzmq/zmq.hpp"
#include "../thirdparty/cppzmq/zmq_addon.hpp"
#include <string>

class LocalClient
{
public:
    LocalClient(const std::string& connectAddr);
    void send(const std::string& str);
    std::string recv();
private:
    zmq::context_t m_context;
    zmq::socket_t m_socket;
};

#endif // LOCALCLIENT_H
