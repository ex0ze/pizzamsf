#include <iostream>
#include "../thirdparty/cxxopts/include/cxxopts.hpp"
#include <pizza-runner/ServerMessageFormat.hpp>
#include <pizza-runner/message/IMessage.h>
#include "LocalClient.h"

#include <pizza-runner/message/JSONMessageCreator.h>
#include <pizza-runner/message/LoadMessage.h>
#include <pizza-runner/message/RunMessage.h>
#include <pizza-runner/message/ReloadMessage.h>
#include <pizza-runner/message/StopMessage.h>
#include <pizza-runner/message/UnloadMessage.h>
#include <pizza-runner/message/RestartMessage.h>
#include <pizza-runner/message/RequestRegularStatisticsMessage.h>
#include <pizza-runner/message/ReplyRegularStatisticsMessage.h>
#include <pizza-runner/stats/RegularStatisticsCollector.h>


namespace pr = pizza::runner;
namespace msg = pr::message;
namespace stats = pr::statistics;

void showStats(msg::ReplyStatisticsMessage * msg)
{
    auto collector = msg->collector();
    if (collector->type() == stats::IStatisticsCollector::Type::Regular)
    {
        auto derived = dynamic_cast<stats::RegularStatisticsCollector*>(collector);
        if (derived->collectedStats().size() == 0)
        {
            std::cout << "\nNo statistics collected\n";
        }
        else
        {
            for (const auto& stat : derived->collectedStats())
            {
                std::cout << "\nStatistic for config " << std::quoted(stat.m_configName) << " :\n";
                std::cout << "\t\tRunning\tAll\n";
                std::cout << "Balancers\t" << stat.m_runningBalancersCount << '\t' << stat.m_allBalancersCount << std::endl;
                std::cout << "Brokers  \t" << stat.m_runningBrokersCount << '\t' << stat.m_allBrokersCount << std::endl;
                std::cout << "Services \t" << stat.m_runningServicesCount << '\t' << stat.m_allServicesCount << std::endl;
                std::cout << "\nRunning time: " << stat.m_runningTime << " s\n";
            }
        }
    }
}

int main(int argc, char* argv[])
{
    std::string description = "Pizza Runner Cli is utility that controls Pizza Runner Daemon";
    cxxopts::Options options("Pizza Runner Cli", std::move(description));
    options.add_options()
        ("load", "Load config file from specified <path>", cxxopts::value<std::string>())
            ("unload", "Stop (if running) and unload config <name> from operating memory", cxxopts::value<std::string>())
                ("run", "Run config <name>", cxxopts::value<std::string>())
                    ("stop", "Stop config <name>", cxxopts::value<std::string>())
                        ("restart", "Stop working config <name> and run it again", cxxopts::value<std::string>())
                            ("reload", "Stop config <name> and reload it from disk", cxxopts::value<std::string>())
                                ("regular-statistics", "Show short statistics for every running config")
                                    ("h,help", "Show this help");
    cxxopts::ParseResult result;
    try
    {
        result = options.parse(argc, argv);
    }
    catch (std::exception& e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    if (result.count("help") > 0)
    {
        std::cout << options.help() << std::endl;
        return 0;
    }
    msg::IMessage::Ptr sendMessage;
    msg::IMessage::Ptr recvMessage;
    LocalClient client(pr::srv::format::kDefaultDaemonAddr);
    if (result.count("load"))
    {
        const auto & confPath = result["load"].as<std::string>();
        sendMessage = msg::RequestLoadMessage::create();
        dynamic_cast<msg::RequestLoadMessage*>(sendMessage.get())->setPath(confPath);

    }
    else if (result.count("unload"))
    {
        const auto& confName = result["unload"].as<std::string>();
        sendMessage = msg::RequestUnloadMessage::create();
        dynamic_cast<msg::RequestUnloadMessage*>(sendMessage.get())->setName(confName);
    }
    else if (result.count("run"))
    {
        const auto& confName = result["run"].as<std::string>();
        sendMessage = msg::RequestRunMessage::create();
        dynamic_cast<msg::RequestRunMessage*>(sendMessage.get())->setName(confName);
    }
    else if (result.count("stop"))
    {
        const auto& confName = result["stop"].as<std::string>();
        sendMessage = msg::RequestStopMessage::create();
        dynamic_cast<msg::RequestStopMessage*>(sendMessage.get())->setName(confName);
    }
    else if (result.count("restart"))
    {
        const auto& confName = result["restart"].as<std::string>();
        sendMessage = msg::RequestRestartMessage::create();
        dynamic_cast<msg::RequestRestartMessage*>(sendMessage.get())->setName(confName);
    }
    else if (result.count("reload"))
    {
        const auto& confName = result["reload"].as<std::string>();
        sendMessage = msg::RequestReloadMessage::create();
        dynamic_cast<msg::RequestReloadMessage*>(sendMessage.get())->setName(confName);
    }
    else if (result.count("regular-statistics"))
    {
        sendMessage = msg::RequestRegularStatisticsMessage::create();
    }
    else
    {
        std::cout << "Error, choose an option\n";
        std::cout << options.help() << std::endl;
        return 1;
    }
    client.send(sendMessage->toJSON());
    auto recvJson = client.recv();
    recvMessage = msg::JSONMessageCreator::instance().fromJSON(recvJson);
    if (!recvMessage)
    {
        std::cout << "Error handling received message from daemon\n";
        return 1;
    }
    if (recvMessage->packet() != msg::IMessage::MessagePacket::Reply)
    {
        std::cout << "Error: received malformed message from daemon\n";
        return 1;
    }
    auto replyPtr = dynamic_cast<msg::IReplyMessage*>(recvMessage.get());
    std::cout << "Reply: Status - " << msg::IReplyMessage::recognizeStatus(replyPtr->status()).value() << std::endl;
    if (replyPtr->action() == msg::IReplyMessage::MessageAction::CollectRegularStatistics || replyPtr->action() == msg::IReplyMessage::MessageAction::CollectVerboseStatistics)
    {
        showStats(dynamic_cast<msg::ReplyStatisticsMessage*>(replyPtr));
    }
    else
        std::cout << "What: " << replyPtr->what() << std::endl;
    return 0;
}
