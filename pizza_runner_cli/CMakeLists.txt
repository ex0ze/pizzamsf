cmake_minimum_required(VERSION 3.14)

project(pizza-runner-cli LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
find_package(Threads)
find_package(pizza-runner)


set(SOURCES
    LocalClient.cpp
    main.cpp
)

set(HEADERS
    LocalClient.h
)

add_executable(pizza-runner-cli ${SOURCES} ${HEADERS})
target_include_directories(pizza-runner-cli PUBLIC ${ZeroMQ_INCLUDE_DIR})
target_include_directories(pizza-runner-cli PRIVATE ${PIZZARUNNER_INCLUDE_DIR})
target_link_libraries(pizza-runner-cli PUBLIC ${ZeroMQ_LIBRARY}
    Threads::Threads
    pizza-runner)
