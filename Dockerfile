FROM ubuntu:20.04
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update -y && \
    apt-get dist-upgrade -y && \
    apt-get install gcc g++ cmake libzmq3-dev libboost-all-dev rapidjson-dev -y
WORKDIR /app/
ADD . .
WORKDIR /app/build/
RUN cmake .. -DCMAKE_BUILD_TYPE=Debug && cmake --build .
