#include <iostream>
#include "../thirdparty/cxxopts/include/cxxopts.hpp"
#include "../thirdparty/cppzmq/zmq.hpp"

#include "from_json.hpp"


using key_pair_t = std::pair<std::array<char, 41>, std::array<char, 41>>;

// public - private
key_pair_t generate_key_pair() {
    key_pair_t result;
    if (zmq_curve_keypair(result.first.data(), result.second.data()) != 0)
        throw std::runtime_error("zmq_curve_keypair");
    return result;
}

void set_key_pair(zmq::socket_t& socket, const key_pair_t& key_pair) {
    socket.set(zmq::sockopt::curve_publickey, key_pair.first.data());
    socket.set(zmq::sockopt::curve_secretkey, key_pair.second.data());
}

int main(int argc, char** argv)
{
    constexpr size_t int_size = sizeof(int);
    std::string description = "Pizza Proxy Balancer is service that:\n"
                              "1) makes possible to run multiple instances of the same service\n"
                              "2) makes task distribution (round robin) across all instances of the same service";
    cxxopts::Options options("Pizza Proxy Balancer", std::move(description));
    options.add_options()
        ("from-json", "Json file path", cxxopts::value<std::string>())
        ("frontend-addr", "Address of the broker (will connect to this)", cxxopts::value<std::string>())
        ("backend-addr", "Address of the balancer (will bind to this, specify this as service reply address)", cxxopts::value<std::string>())
        ("service-name", "Name of the service, for which balancing will be performed", cxxopts::value<std::string>())
        ("h,help", "Show this help");
    cxxopts::ParseResult result;
    try {
        result = options.parse(argc, argv);
    }
    catch (std::exception& e) {
        std::cout << "Error: " << e.what() << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }

    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        return 0;
    }

    zmq::context_t ctx;
    zmq::socket_t dealer_in(ctx, zmq::socket_type::dealer);
    zmq::socket_t dealer_out(ctx, zmq::socket_type::dealer);

    if (result.count("from-json")) {
        try {
            auto args = from_json(result["from-json"].as<std::string>());
            if (args.encryption()) {
                dealer_in.set(zmq::sockopt::curve_serverkey, *args.broker_public_key());
                set_key_pair(dealer_in, generate_key_pair());

                dealer_out.set(zmq::sockopt::curve_server, true);
                dealer_out.set(zmq::sockopt::curve_secretkey, *args.balancer_private_key());
            }
            dealer_in.set(zmq::sockopt::routing_id, args.service_name());
            dealer_in.connect(args.broker_addr());
            dealer_out.bind(args.addr());
            std::cout << "Starting proxy balancer for service " << args.service_name() << std::endl;
            std::cout << "Broker addr: " << args.broker_addr() << " ; Balancer addr: " << args.addr() << std::endl;
            zmq::proxy(dealer_in, dealer_out);
            return 0;
        }
        catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
            return 1;
        }
    }

    if (result.count("frontend-addr") == 0 || result.count("backend-addr") == 0 || result.count("service-name") == 0) {
        std::cout << "Missing required fields" << std::endl;
        std::cout << options.help() << std::endl;
        return 1;
    }
    else {
        try {
            const auto& service_name = result["service-name"].as<std::string>();
            const auto& frontend_addr = result["frontend-addr"].as<std::string>();
            const auto& backend_addr = result["backend-addr"].as<std::string>();
            dealer_in.set(zmq::sockopt::routing_id, service_name);
            dealer_in.connect(frontend_addr);
            dealer_out.bind(backend_addr);
            std::cout << "Starting proxy balancer for service " << service_name << std::endl;
            std::cout << "Broker addr: " << frontend_addr << " ; Balancer addr: " << backend_addr << std::endl;
            zmq::proxy(dealer_in, dealer_out);
        }
        catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
            return 1;
        }
    }
    return 0;
}
