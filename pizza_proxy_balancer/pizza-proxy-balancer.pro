TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../build/
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
SOURCES += \
        main.cpp

HEADERS += \
    pizza-proxy-balancer.hpp

LIBS += -lzmq
