TEMPLATE = app
CONFIG += console c++17 thread
CONFIG -= app_bundle
CONFIG -= qt
DESTDIR = ../build/
MOC_DIR = ./moc
OBJECTS_DIR = ./obj
SOURCES += \
        main.cpp

LIBS += -lzmq

HEADERS +=
