/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <thread>
#include <atomic>
#include <../thirdparty/cxxopts/include/cxxopts.hpp>
#include <../thirdparty/cppzmq/zmq.hpp>
#include <../thirdparty/cppzmq/zmq_addon.hpp>

#include "from_json.hpp"

void handle_request(zmq::socket_t& socket, std::vector<zmq::message_t>& messages, std::size_t parts) {
    std::cout << __func__ << std::endl;
    socket.send(std::move(messages[2]), zmq::send_flags::sndmore);
    socket.send(std::move(messages[0]),
        parts == 2 ? zmq::send_flags::none : zmq::send_flags::sndmore);
    
    for (std::size_t part = 3; part < parts; ++part)
        socket.send(std::move(messages[part]),
            (part == parts - 1) ? zmq::send_flags::none : zmq::send_flags::sndmore);
}

void handle_response(zmq::socket_t& socket, std::vector<zmq::message_t>& messages, std::size_t parts) {
    std::cout << __func__ << std::endl;
    if (parts < 2) return;
    socket.send(std::move(messages[1]), zmq::send_flags::sndmore);
    zmq::message_t delim_frame;
    delim_frame.rebuild(0);
    socket.send(std::move(delim_frame), parts == 2 ? zmq::send_flags::none : zmq::send_flags::sndmore);
    for (std::size_t part = 2; part < parts; ++part) 
        socket.send(std::move(messages[part]), part == parts - 1 ? zmq::send_flags::none : zmq::send_flags::sndmore);
}

void do_route(zmq::socket_t& socket) {
    zmq::pollitem_t poll_items[] = {{ static_cast<void*>(socket), 0, ZMQ_POLLIN, 0 }};
    std::vector<zmq::message_t> messages;
    messages.resize(20);
    while (true) {
        zmq::poll(poll_items, std::size(poll_items), -1);
        if (poll_items[0].revents & ZMQ_POLLIN == 0) continue;
        std::size_t parts;
        try {
            auto maybe_parts = zmq::recv_multipart_n(socket, messages.begin(), messages.size());
            std::cout << "recv" << std::endl;
            if (!maybe_parts) continue;
            parts = *maybe_parts;
        }
        catch (...) {
            continue;
        }
        if (parts >= 2 && messages[1].size() == 0)
            handle_request(socket, messages, parts);
        else
            handle_response(socket, messages, parts);
    }
}

int main(int argc, char* argv[])
{
    cxxopts::Options options("Pizza broker", "Service that route packages between other services");
    options.allow_unrecognised_options().add_options()
    ("from-json", "Json file path", cxxopts::value<std::string>())
    ("addr", "Broker bind address", cxxopts::value<std::string>())
    ("v,verbose", "Output pps rate to stdout", cxxopts::value<bool>()->default_value("false"))
    ("h,help", "Show this help");
    auto result = options.parse(argc, argv);
    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        return 0;
    }
    if (result.count("from-json")) {
        try {
            auto args = from_json(result["from-json"].as<std::string>());
            zmq::context_t ctx(1);
            zmq::socket_t socket(ctx, zmq::socket_type::router);
            if (args.encryption()) {
                auto maybe_priv_key = args.broker_private_key();
                if (!maybe_priv_key)
                    throw std::runtime_error("Missing key");
                auto priv_key = maybe_priv_key.value();
                if (priv_key.size() != 40)
                    throw std::runtime_error("Wrong key length");
                socket.set(zmq::sockopt::curve_server, true);
                socket.set(zmq::sockopt::curve_secretkey, priv_key);
            }
            socket.bind(args.addr());
            do_route(socket);
        }
        catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
            return 1;
        }
    }
    if (result.count("addr") == 0) {
        std::cout << "Missing --addr required field\n";
        std::cout << options.help() << std::endl;
        return 1;
    }
    const auto& bind_addr = result["addr"].as<std::string>();
    bool verbose = result["verbose"].as<bool>();
    try {
        zmq::context_t ctx(1);
        zmq::socket_t socket(ctx, zmq::socket_type::router);
        socket.bind(bind_addr);
        do_route(socket);
    }
    catch (const std::exception& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
    }

    return 0;
}
